package com.banuba.sdk.offscreen;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;

@Keep
public interface ImageProcessedListener {
    void onImageProcessed(@NonNull ImageProcessResult result);
}
