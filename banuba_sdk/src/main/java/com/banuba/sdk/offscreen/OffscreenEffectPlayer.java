package com.banuba.sdk.offscreen;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.media.Image;
import android.os.Handler;
import android.util.Size;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.banuba.sdk.effect_player.EffectPlayer;
import com.banuba.sdk.effect_player.JsCallback;
import com.banuba.sdk.types.FullImageData;

@Keep
public class OffscreenEffectPlayer {
    private final OffscreenPlayerHandler mHandler;
    private final OffscreenSimpleConfig mConfig;

    public OffscreenEffectPlayer(@NonNull Context context, @NonNull OffscreenEffectPlayerConfig config, @NonNull String key) {
        mConfig = config.getSimpleConfig();
        mHandler = new OffscreenPlayerThread(context.getApplicationContext(), config, key).startAndGetHandler();
    }

    public OffscreenEffectPlayer(@NonNull Context context, @NonNull EffectPlayer player, @NonNull Size size, @NonNull OffscreenSimpleConfig config) {
        mConfig = config;
        mHandler = new OffscreenPlayerThread(context.getApplicationContext(), player, size, config).startAndGetHandler();
    }

    /**
     * Sets ImageProcessListener
     * The callback will be invoked on handler's thread.
     * Can be used together with surfaceTexture.
     * In this case, OffscreenEffectPlayer will render to SurfaceTexture and send image to listener
     * <p>
     * Passing NULL/NULL will disable prepare and send Buffers to listener
     *
     * @param listener The listener to use, or null to remove the listener.
     * @param handler  The handler on which the listener should be invoked, or null to remove the listener
     */
    public void setImageProcessListener(@Nullable ImageProcessedListener listener, @Nullable Handler handler) {
        mHandler.sendImage2ProcessListener(listener, handler);
    }

    /**
     * Sets SurfaceTexture
     * Can be used together with ImageProcessListener.
     * In this case OffscreenEffectPlayer will render to SurfaceTexture and send image to listener
     * <p>
     * Passing NULL will disable render to SurfaceTexture
     *
     * @param surfaceTexture The surfaceTexture to use, or null to remove.
     */
    public void setSurfaceTexture(@Nullable SurfaceTexture surfaceTexture) {
        mHandler.sendSurfaceTexture(surfaceTexture);
    }

    /**
     * Send Image to process
     * Image will be closed inside OffscreenEffectPlayer
     *
     * @param image       Android image to process
     * @param orientation describes current image orientation
     */
    public void processImage(@NonNull Image image, @NonNull ImageOrientation orientation) {
        processImage(image, orientation, null);
    }

    /**
     * Send Image to process
     * Image will be closed inside OffscreenEffectPlayer
     *
     * @param image             Android image to process
     * @param orientation       describes current image orientation
     * @param outputImageFormat describes output Image Format (RGBA_8888 or YUV_420_888)
     */
    public void processImage(@NonNull Image image, @NonNull ImageOrientation orientation, @Nullable Integer outputImageFormat) {
        final int inputFormat = image.getFormat();
        final boolean inputOk = inputFormat == PixelFormat.RGBA_8888 || inputFormat == ImageFormat.YUV_420_888;
        final boolean outputOk = outputImageFormat == null || outputImageFormat == PixelFormat.RGBA_8888 || outputImageFormat == ImageFormat.YUV_420_888;

        if (inputOk && outputOk) {
            mHandler.sendImage2Process(image, orientation, outputImageFormat, image.getTimestamp());
        } else {
            throw new RuntimeException("Only PixelFormat.RGBA_8888(0x1) or ImageFormat.YUV_420_888(0x23) supported!"
                                       + " Current Input = 0x" + Integer.toHexString(inputFormat) + " Output = " + (outputImageFormat != null ? Integer.toHexString(outputImageFormat) : "null"));
        }
    }

    /**
     * Send FullImageData to process. FullImageData is a container for the Image object or buffers representing image in a specific format.
     *
     * @param fullImageData     Instance of FullImageData object
     * @param releaseCallback   is called by OEP when image stored in fullImageData can be released. If releaseCallback is passed
     *                          then Image data isn't copied to improve performance. If a releaseCallback is passed and an Image
     *                          is used, undefined behavior may occur when the ImageReader is closed and then opened again. In order
     *                          to avoid this, it is necessary to call playbackStop before closing the camera and playbackPlay after opening it.
     * @param timestamp         lets you associate frame with the timestamp in order to identify it at ImageProcessedListener call
     */
    public void processFullImageData(@NonNull FullImageData fullImageData, @Nullable ReleaseCallback releaseCallback, long timestamp) {
        internalProcessFullImageData(fullImageData, releaseCallback, null, timestamp, true);
    }

    /**
     * Send FullImageData to process. FullImageData is a container for the Image object or buffers representing image in a specific format.
     *
     * @param fullImageData     Instance of FullImageData object
     * @param releaseCallback   is called by OEP when image stored in fullImageData can be released. If releaseCallback is passed
     *                          then Image data isn't copied to improve performance. If a releaseCallback is passed and an Image
     *                          is used, undefined behavior may occur when the ImageReader is closed and then opened again. In order
     *                          to avoid this, it is necessary to call playbackStop before closing the camera and playbackPlay after opening it.
     * @param outputImageFormat PixelFormat.RGBA_8888 or ImageFormat.YUV_420_888 used as result image format
     * @param timestamp         lets you associate frame with the timestamp in order to identify it at ImageProcessedListener call
     */
    public void processFullImageData(@NonNull FullImageData fullImageData, @Nullable ReleaseCallback releaseCallback, @Nullable Integer outputImageFormat, long timestamp) {
        internalProcessFullImageData(fullImageData, releaseCallback, outputImageFormat, timestamp, true);
    }


    /**
     * Meaning and parameters are the same as for processFullImageData but in that case the image will wait in the queue until processed,
     * i.e. every image is processed without drops even if another image is being processed at this moment.
     */
    @VisibleForTesting
    public void processFullImageDataNoSkip(@NonNull FullImageData fullImageData, @Nullable ReleaseCallback releaseCallback, @Nullable Integer outputImageFormat, long timestamp) {
        internalProcessFullImageData(fullImageData, releaseCallback, outputImageFormat, timestamp, false);
    }

    @VisibleForTesting
    public void processFullImageDataNoSkip(@NonNull FullImageData fullImageData, @Nullable ReleaseCallback releaseCallback, long timestamp) {
        internalProcessFullImageData(fullImageData, releaseCallback, null, timestamp, false);
    }

    private void internalProcessFullImageData(@NonNull FullImageData fullImageData, @Nullable ReleaseCallback releaseCallback, @Nullable Integer outputImageFormat, long timestamp, boolean isFrameSkipable) {
        final int inputFormat = fullImageData.getPixelFormat();
        final boolean inputOk = inputFormat == PixelFormat.RGBA_8888 || inputFormat == ImageFormat.YUV_420_888;
        final boolean outputOk = outputImageFormat == null || outputImageFormat == PixelFormat.RGBA_8888 || outputImageFormat == ImageFormat.YUV_420_888;

        if (inputOk && outputOk) {
            mHandler.sendFullImage2Process(fullImageData, releaseCallback, outputImageFormat, timestamp, isFrameSkipable);
        } else {
            throw new RuntimeException("Only PixelFormat.RGBA_8888(0x1) or ImageFormat.YUV_420_888(0x23) supported!"
                                       + " Current Input = 0x" + Integer.toHexString(inputFormat) + " Output = " + (outputImageFormat != null ? Integer.toHexString(outputImageFormat) : "null"));
        }
    }

    /**
     * Load an effect to the effect player.
     *
     * @param effectName the name of an effect (folder) in the app resource storage
     */
    public void loadEffect(@NonNull String effectName) {
        mHandler.sendLoadEffect(effectName);
    }

    /**
     * Unload active effect. The effect stays in the cache so that another loading of the same effect is faster.
     */
    public void unloadEffect() {
        mHandler.sendUnloadEffect();
    }

    /**
     * Allows calling methods defined in the script of the active effect to configure the effect or manage its state.
     * This method is obsolete, see evalJs.
     *
     * @param method    - the name of the js method
     * @param parameter - the parameter to pass to the method
     */
    public void callJsMethod(@NonNull String method, @NonNull String parameter) {
        mHandler.sendCallJSMethod(method, parameter);
    }

    /**
     * Allows execute scripts (calling methods) or load JS modules to the environment of active effect
     *
     * @param script - js script with escaped quotes
     * @param resultCallback - the callback called after script's execution completed
     *
     * @example: evalJs("Background = require('bnb_js/background')", null);
     *           evalJs("Background.blur(0.8)", null)
     */
    public void evalJs(@NonNull String script, @Nullable JsCallback resultCallback) {
        mHandler.sendEvalJs(script, resultCallback);
    }

    /**
     * Stop processing and release OffscreenEffectPlayer resources
     */
    public void release() {
        mHandler.sendShutdown();
    }

    /**
     * Switching to active state attempt. Possible from paused or stopped state and has no effect if effect
     * playback is already active. Playback resumes from the position saved before `playbackPause` call.
     */
    public void playbackPlay() {
        mHandler.sendPlaybackPlay();
    }

    /**
     * Suspend current playback attempt. The recognizer thread is stopped and all the video textures and audio
     * units playback is stopped as well. Effect player doesn't react on processImage or processFullImageData
     * calls in suspended state except asynchronous-inconsistent mode.
     */
    public void playbackPause() {
        mHandler.sendPlaybackPause();
    }

    /**
     * Switch to inactive state. In addition to pause clears recognizer's buffer. The next switch to active state
     * will result in total rerun of active effect which means that it will be started from the very beginning.
     * Call this method when you stop input from camera (e.g. went in background or switch).
     */
    public void playbackStop() {
        mHandler.sendPlaybackStop();
    }

    /**
     * Return the current OEP configuration
     */
    @NonNull
    public OffscreenSimpleConfig getConfig() {
        return mConfig;
    }
}
