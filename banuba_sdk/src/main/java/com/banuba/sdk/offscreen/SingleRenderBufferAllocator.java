package com.banuba.sdk.offscreen;

import androidx.annotation.NonNull;

import com.banuba.sdk.internal.gl.RenderBuffer;

public class SingleRenderBufferAllocator {
    private RenderBuffer mRenderBuffer;

    @NonNull
    RenderBuffer getRenderBuffer(int width, int height) {
        if (mRenderBuffer == null) {
            mRenderBuffer = RenderBuffer.prepareFrameBuffer(width, height);
        } else {
            if (mRenderBuffer.getWidth() != width && mRenderBuffer.getHeight() != height) {
                mRenderBuffer.clear();
                mRenderBuffer = RenderBuffer.prepareFrameBuffer(width, height);
            }
        }
        return mRenderBuffer;
    }

    void clear() {
        if (mRenderBuffer != null) {
            mRenderBuffer.clear();
            mRenderBuffer = null;
        }
    }
}
