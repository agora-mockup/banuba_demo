package com.banuba.sdk.offscreen;

import android.util.Size;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.banuba.sdk.effect_player.EffectPlayerConfiguration;
import com.banuba.sdk.effect_player.NnMode;
import com.banuba.sdk.recognizer.FaceSearchMode;

/**
 * Encapsulates info about Offscreen Effect Player ({@link OffscreenEffectPlayer}) Configuration (Full).
 */
@Keep
public class OffscreenEffectPlayerConfig {
    private final int fxWidth;
    private final int fxHeight;
    private final NnMode nnEnable;
    private final FaceSearchMode faceSearch;
    private final boolean jsDebuggerEnable;
    private final boolean manualAudio;

    @Nullable
    private final BufferAllocator bufferAllocator;
    private final boolean debugSaveFrames;
    private final int debugSaveFramesDivider;

    private OffscreenEffectPlayerConfig(Builder builder) {
        this.fxWidth = builder.fxWidth;
        this.fxHeight = builder.fxHeight;
        this.nnEnable = builder.nnEnable;
        this.faceSearch = builder.faceSearch;
        this.jsDebuggerEnable = builder.jsDebuggerEnable;
        this.manualAudio = builder.manualAudio;
        this.bufferAllocator = builder.bufferAllocator;
        this.debugSaveFrames = builder.debugSaveFrames;
        this.debugSaveFramesDivider = builder.debugSaveFramesDivider;
    }

    int getVerticalPictureWidth() {
        return Math.min(fxWidth, fxHeight);
    }

    int getMinDimension() {
        return Math.min(fxWidth, fxHeight);
    }

    int getMaxDimension() {
        return Math.max(fxWidth, fxHeight);
    }

    int getVerticalPictureHeight() {
        return Math.max(fxWidth, fxHeight);
    }

    @NonNull
    EffectPlayerConfiguration getEffectPlayerConfig() {
        return new EffectPlayerConfiguration(getVerticalPictureWidth(), getVerticalPictureHeight(), nnEnable, faceSearch, jsDebuggerEnable, manualAudio);
    }

    /**
     * Return {@link OffscreenSimpleConfig} instance based on {@link EffectPlayerConfiguration} parameters.
     *
     * @return {@link OffscreenSimpleConfig} instance.
     */
    @NonNull
    public OffscreenSimpleConfig getSimpleConfig() {
        return OffscreenSimpleConfig.newBuilder(bufferAllocator)
            .setDebugSaveFrames(debugSaveFrames)
            .setDebugSaveFramesDivider(debugSaveFramesDivider)
            .build();
    }

    /**
     * Create new {@link Builder} instance.
     *
     * @param size the {@link Size} that {@link OffscreenEffectPlayer} uses for drawing.
     * @param allocator {@link BufferAllocator} instance.
     * @return {@link Builder} instance.
     */
    public static Builder newBuilder(@NonNull Size size, @Nullable BufferAllocator allocator) {
        return new Builder(size.getWidth(), size.getHeight(), allocator);
    }

    @Keep
    public static class Builder {
        private final int fxWidth;
        private final int fxHeight;
        private NnMode nnEnable = NnMode.ENABLE;
        private FaceSearchMode faceSearch = FaceSearchMode.GOOD;
        private boolean jsDebuggerEnable = false;
        private boolean manualAudio = false;
        @Nullable
        private final BufferAllocator bufferAllocator;
        private boolean debugSaveFrames = false;
        private int debugSaveFramesDivider = 100;

        public Builder(int width, int height, @Nullable BufferAllocator allocator) {
            this.fxWidth = width;
            this.fxHeight = height;
            this.bufferAllocator = allocator;
        }

        /**
         * Set {@link NnMode} parameter value.
         *
         * @param nnEnable {@link NnMode} value.
         * @return {@link Builder} instance.
         */
        @NonNull
        public Builder setNNMode(@NonNull NnMode nnEnable) {
            this.nnEnable = nnEnable;
            return this;
        }

        /**
         * Set {@link FaceSearchMode} parameter value.
         *
         * @param faceSearchMode {@link FaceSearchMode} value.
         * @return {@link Builder} instance.
         */
        @NonNull
        public Builder setFaceSearchMode(@NonNull FaceSearchMode faceSearchMode) {
            this.faceSearch = faceSearchMode;
            return this;
        }

        /**
         * Set JS debugger enabled flag value.
         *
         * @param jsDebuggerEnable JS debugger enabled flag value.
         * @return {@link Builder} instance.
         */
        @NonNull
        public Builder setJsDebuggerEnable(boolean jsDebuggerEnable) {
            this.jsDebuggerEnable = jsDebuggerEnable;
            return this;
        }

        /**
         * Set debug save frames flag.
         *
         * @param debugSaveFrames debug save frames flag value.
         * @return {@link Builder} instance.
         */
        @NonNull
        public Builder setDebugSaveFrames(boolean debugSaveFrames) {
            this.debugSaveFrames = debugSaveFrames;
            return this;
        }

        /**
         * Set debug save frames divider.
         *
         * @param debugSaveFramesDivider debug save frames divider value.
         * @return {@link Builder} instance.
         */
        @NonNull
        public Builder setDebugSaveFramesDivider(int debugSaveFramesDivider) {
            this.debugSaveFramesDivider = debugSaveFramesDivider;
            return this;
        }

        /**
         * Build the {@link OffscreenEffectPlayerConfig} object.
         *
         * @return {@link OffscreenEffectPlayerConfig} object.
         */
        @NonNull
        public OffscreenEffectPlayerConfig build() {
            return new OffscreenEffectPlayerConfig(this);
        }
    }
}
