package com.banuba.sdk.offscreen;

import android.opengl.Matrix;

import androidx.annotation.NonNull;

public class ImageTransform {
    static final int FLIP_NONE = 0;
    static final int FLIP_Y = 1;
    static final int FLIP_X = 2;
    static final int FLIP_XY = 3;

    private final float[] mMatrixConvert = new float[16];
    private final float[] mMatrixScreen = new float[16];
    private final int mConverterMode;


    ImageTransform(int convertAngle, int convertFlip, int converterSetup, int screenAngle, int screenFlip) {
        calculateCameraMatrixFlip(mMatrixConvert, convertAngle, convertFlip);
        calculateCameraMatrixFlip(mMatrixScreen, screenAngle, screenFlip);
        mConverterMode = converterSetup;
    }

    private static void calculateCameraMatrixFlip(float[] matrix, int angle, int flip) {
        final float[] rotate = new float[16];
        final float[] transPos = new float[16];
        final float[] transNeg = new float[16];
        final float[] temp = new float[16];
        final float[] temp2 = new float[16];
        final float[] scale = new float[16];

        Matrix.setIdentityM(scale, 0);
        if (flip == FLIP_X) {
            Matrix.scaleM(scale, 0, -1.0f, 1.0f, 1.0f);
        } else if (flip == FLIP_Y) {
            Matrix.scaleM(scale, 0, 1.0f, -1.0f, 1.0f);
        } else if (flip == FLIP_XY) {
            Matrix.scaleM(scale, 0, -1.0f, -1.0f, 1.0f);
        }

        Matrix.setIdentityM(transPos, 0);
        Matrix.setIdentityM(transNeg, 0);
        Matrix.setIdentityM(rotate, 0);

        Matrix.translateM(transPos, 0, 0.5f, 0.5f, 0);
        Matrix.translateM(transNeg, 0, -0.5f, -0.5f, 0);

        Matrix.setRotateM(rotate, 0, angle, 0, 0, 1);

        Matrix.multiplyMM(temp, 0, transPos, 0, rotate, 0);
        Matrix.multiplyMM(temp2, 0, temp, 0, scale, 0);
        Matrix.multiplyMM(matrix, 0, temp2, 0, transNeg, 0);
    }

    @NonNull
    public float[] getScreenMatrix() {
        return mMatrixScreen;
    }

    @NonNull
    public float[] getConvertMatrix() {
        return mMatrixConvert;
    }

    public int getConverterMode() {
        return mConverterMode;
    }
}
