
package com.banuba.sdk.offscreen;

import android.opengl.GLES20;
import android.util.Size;
import android.util.Log;

import androidx.annotation.NonNull;

import com.banuba.sdk.internal.gl.GlUtils;
import com.banuba.sdk.internal.gl.RenderBuffer;

import java.nio.ByteBuffer;

import static com.banuba.sdk.internal.gl.GlUtils.COORDS_PER_VERTEX;
import static com.banuba.sdk.internal.gl.GlUtils.COORDS_UV_PER_TEXTURE;
import static com.banuba.sdk.internal.gl.GlUtils.TEXTURE_STRIDE;
import static com.banuba.sdk.internal.gl.GlUtils.VERTEX_STRIDE;

public class YUVConverterMod {
    public static final int CONV_MODE_STEP_X = 0;
    public static final int CONV_MODE_STEP_NEG_X = 1;
    public static final int CONV_MODE_STEP_Y = 2;
    public static final int CONV_MODE_STEP_NEG_Y = 3;

    private static final float[] RECTANGLE_VERTEX = new float[] {
        -1f, -1f, 0, // 0 bottom left
        1f,
        -1f,
        0, // 1 bottom right
        -1f,
        1f,
        0, // 2 top left
        1f,
        1f,
        0, // 3 top right
    };

    private static final float[] RECTANGLE_TEXTURE_UV = {
        0.0f, 0.0f, // 0 bottom left
        1.0f,
        0.0f, // 1 bottom right
        0.0f,
        1.0f, // 2 top left
        1.0f,
        1.0f // 3 top right
    };


    private static final String SHADER_VEC = " "
                                             + "  uniform mat4 uTexMatrix;                        \n"
                                             + "  attribute vec4 a_position;                      \n"
                                             + "  attribute vec2 a_texCoord;                      \n"
                                             + "  varying vec2 tc;                                \n"
                                             + "  void main()                                     \n"
                                             + "  {                                               \n"
                                             + "     gl_Position = a_position;                    \n"
                                             + "     vec4 texCoord = vec4(a_texCoord, 0.0, 1.0);  \n"
                                             + "     tc = (uTexMatrix * texCoord).xy;             \n"
                                             + "  }                                               \n";


    private static final String FRAGMENT_SHADER = ""
                                                  + "precision highp float;  \n"
                                                  + " uniform vec2 xUnit;    \n"
                                                  + " varying vec2 tc;       \n"
                                                  + " uniform vec4 coeffs;   \n"
                                                  + " uniform sampler2D s_baseMap;  \n"
                                                  + " void main() {\n"
                                                  + "  gl_FragColor.r = coeffs.a + dot(coeffs.rgb,\n"
                                                  + "      texture2D(s_baseMap, tc - 1.5 * xUnit).rgb);\n"
                                                  + "  gl_FragColor.g = coeffs.a + dot(coeffs.rgb,\n"
                                                  + "      texture2D(s_baseMap, tc - 0.5 * xUnit).rgb);\n"
                                                  + "  gl_FragColor.b = coeffs.a + dot(coeffs.rgb,\n"
                                                  + "      texture2D(s_baseMap, tc + 0.5 * xUnit).rgb);\n"
                                                  + "  gl_FragColor.a = coeffs.a + dot(coeffs.rgb,\n"
                                                  + "      texture2D(s_baseMap, tc + 1.5 * xUnit).rgb);\n"
                                                  + "}\n";

    // Reduced Range
    private static final float[] yCoeffs = new float[] {0.256788f, 0.504129f, 0.0979059f, 0.0627451f};
    private static final float[] uCoeffs = new float[] {-0.148223f, -0.290993f, 0.439216f, 0.501961f};
    private static final float[] vCoeffs = new float[] {0.439216f, -0.367788f, -0.0714274f, 0.501961f};

    // Full
    //    private static final float[] yCoeffs = new float[]{0.299f, 0.587f, 0.114f, 0.0f};
    //    private static final float[] uCoeffs = new float[]{-0.168736f, -0.331264f, 0.5f, 0.5f};
    //    private static final float[] vCoeffs = new float[]{0.5f, 0.418688f, -0.0813124f, 0.5f};

    private final RenderBuffer mFrameBufferYUV;

    private final int mVertexCount;

    private final Size mFrameSizeYUV;
    private final Size mPlaneSizeY;
    private final Size mPlaneSizeUV;
    private final Size mOutputFrameSize;

    private final int[] mVBO;

    private final int mProgramHandle;
    private final int mAttributePosition;
    private final int mAttributeTextureCoord;
    private final int mUniformSampler;
    private final int mUniformTextureMatrix;

    private final int mTextureLocation;
    private final int mCoeffsLocation;

    public YUVConverterMod(int frameW, int frameH) {
        int stride = (frameW + 7) & ~7;
        mFrameSizeYUV = new Size(stride / 4, frameH + frameH);
        mPlaneSizeY = new Size(frameW / 4, frameH);
        mPlaneSizeUV = new Size(frameW / 8, frameH / 2);
        mFrameBufferYUV = RenderBuffer.prepareFrameBuffer(mFrameSizeYUV.getWidth(), mFrameSizeYUV.getHeight());
        mOutputFrameSize = new Size(frameW, frameH);

        mVBO = GlUtils.setupVertexTextureBuffers(RECTANGLE_VERTEX, RECTANGLE_TEXTURE_UV);
        mProgramHandle = GlUtils.loadProgram(SHADER_VEC, FRAGMENT_SHADER);

        mAttributePosition = GLES20.glGetAttribLocation(mProgramHandle, "a_position");
        mAttributeTextureCoord = GLES20.glGetAttribLocation(mProgramHandle, "a_texCoord");

        mUniformTextureMatrix = GLES20.glGetUniformLocation(mProgramHandle, "uTexMatrix");
        mUniformSampler = GLES20.glGetUniformLocation(mProgramHandle, "s_baseMap");

        mTextureLocation = GLES20.glGetUniformLocation(mProgramHandle, "xUnit");
        mCoeffsLocation = GLES20.glGetUniformLocation(mProgramHandle, "coeffs");

        mVertexCount = RECTANGLE_VERTEX.length / COORDS_PER_VERTEX;

        int errorCode = GLES20.glGetError();
        if (errorCode != GLES20.GL_NO_ERROR) {
            Log.w("YUVConverter.create", "OpenGL error code: " + errorCode);
        }
    }

    // NOTE: YUV reduced range coefficients is used, BT.601 420v (video)
    public void convert(int inputTextureId, @NonNull ByteBuffer output, @NonNull float[] textureMatrix, int converterMode) {
        GLES20.glUseProgram(mProgramHandle);

        // In cases where blending was not turned off at the end of the effect
        GLES20.glDisable(GLES20.GL_BLEND);

        // Vertex Shader Buffers
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mVBO[0]);
        GLES20.glVertexAttribPointer(mAttributePosition, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, VERTEX_STRIDE, 0);
        GLES20.glEnableVertexAttribArray(mAttributePosition);

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mVBO[1]);
        GLES20.glVertexAttribPointer(mAttributeTextureCoord, COORDS_UV_PER_TEXTURE, GLES20.GL_FLOAT, false, TEXTURE_STRIDE, 0);
        GLES20.glEnableVertexAttribArray(mAttributeTextureCoord);

        // Vertex Shader - Uniforms
        GLES20.glUniformMatrix4fv(mUniformTextureMatrix, 1, false, textureMatrix, 0);

        // Fragment Shader - Texture
        GlUtils.setupSampler(0, mUniformSampler, inputTextureId, false);

        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBufferYUV.getFrameBufferId());

        float stepX = 0;
        float stepY = 0;

        // TODO Ideally mode can be replaced by calculation from texture matrix
        if (converterMode == CONV_MODE_STEP_X) {
            stepX = 1f / mOutputFrameSize.getWidth();
        } else if (converterMode == CONV_MODE_STEP_NEG_X) {
            stepX = -1f / mOutputFrameSize.getWidth();
        } else if (converterMode == CONV_MODE_STEP_Y) {
            stepY = 1f / mOutputFrameSize.getWidth();
        } else if (converterMode == CONV_MODE_STEP_NEG_Y) {
            stepY = -1f / mOutputFrameSize.getWidth();
        }

        GLES20.glUniform4fv(mCoeffsLocation, 1, yCoeffs, 0);
        GLES20.glUniform2f(mTextureLocation, stepX, stepY);
        GLES20.glViewport(0, 0, mPlaneSizeY.getWidth(), mPlaneSizeY.getHeight());
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, mVertexCount);

        stepX *= 2.0f;
        stepY *= 2.0f;

        GLES20.glUniform4fv(mCoeffsLocation, 1, uCoeffs, 0);
        GLES20.glUniform2f(mTextureLocation, stepX, stepY);
        GLES20.glViewport(0, mPlaneSizeY.getHeight(), mPlaneSizeUV.getWidth(), mPlaneSizeUV.getHeight());
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, mVertexCount);

        GLES20.glUniform4fv(mCoeffsLocation, 1, vCoeffs, 0);
        GLES20.glUniform2f(mTextureLocation, stepX, stepY);
        GLES20.glViewport(0, mPlaneSizeY.getHeight() + mPlaneSizeUV.getHeight(), mPlaneSizeUV.getWidth(), mPlaneSizeUV.getHeight());
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, mVertexCount);

        GLES20.glReadPixels(0, 0, mFrameSizeYUV.getWidth(), mFrameSizeYUV.getHeight(), GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, output);

        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
        GLES20.glDisableVertexAttribArray(mAttributeTextureCoord);
        GLES20.glDisableVertexAttribArray(mAttributePosition);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
        GLES20.glUseProgram(0);

        int errorCode = GLES20.glGetError();
        if (errorCode != GLES20.GL_NO_ERROR) {
            Log.w("YUVConverter.convert", "OpenGL error code: " + errorCode);
        }
    }

    public int getWidth() {
        return mOutputFrameSize.getWidth();
    }

    public int getHeight() {
        return mOutputFrameSize.getHeight();
    }

    public void clear() {
        mFrameBufferYUV.clear();
        GLES20.glDeleteProgram(mProgramHandle);
        GLES20.glDeleteBuffers(mVBO.length, mVBO, 0);
    }
}
