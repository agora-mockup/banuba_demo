package com.banuba.sdk.offscreen;

import android.graphics.SurfaceTexture;
import android.media.Image;
import android.os.Handler;
import android.os.Message;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.banuba.sdk.effect_player.JsCallback;
import com.banuba.sdk.internal.WeakHandler;
import com.banuba.sdk.internal.utils.Logger;
import com.banuba.sdk.types.FullImageData;

public class OffscreenPlayerHandler extends WeakHandler<OffscreenPlayerThread> {
    private static final int MSG_SHUTDOWN = 0;
    private static final int MSG_LOAD_EFFECT = 1;
    private static final int MSG_UNLOAD_EFFECT = 2;
    private static final int MSG_CAMERA_IMAGE_PROCESS = 3;
    private static final int MSG_FULL_IMAGE_DATA = 4;
    private static final int MSG_CALL_JS_METHOD = 5;
    private static final int MSG_SET_LISTENER = 6;
    private static final int MSG_SET_SURFACE = 7;
    private static final int MSG_EVAL_JS = 8;
    private static final int MSG_PLAYBACK_PLAY = 9;
    private static final int MSG_PLAYBACK_PAUSE = 10;
    private static final int MSG_PLAYBACK_STOP = 11;

    OffscreenPlayerHandler(@NonNull OffscreenPlayerThread baseWorkThread) {
        super(baseWorkThread);
    }

    private static class ProcessAndroidCameraImageArgBundle {
        private final Image androidImage;
        private final ImageOrientation orientation;
        private final long timestamp;
        private final Integer outputImageFormat;

        public ProcessAndroidCameraImageArgBundle(@NonNull Image androidImage, @NonNull ImageOrientation orientation, @Nullable Integer outputImageFormat, long timestamp) {
            this.androidImage = androidImage;
            this.orientation = orientation;
            this.outputImageFormat = outputImageFormat;
            this.timestamp = timestamp;
        }
    }

    private static class ProcessFullImageDataArgBundle {
        @NonNull
        private final FullImageData data;

        @Nullable
        private final ReleaseCallback releaseCallback;

        @Nullable
        private final Integer outputImageFormat;

        private final long timestamp;
        private final boolean isFrameSkipable;

        public ProcessFullImageDataArgBundle(@NonNull FullImageData data, @Nullable ReleaseCallback releaseCallback, @Nullable Integer outputImageFormat, long timestamp, boolean isFrameSkipable) {
            this.data = data;
            this.releaseCallback = releaseCallback;
            this.outputImageFormat = outputImageFormat;
            this.timestamp = timestamp;
            this.isFrameSkipable = isFrameSkipable;
        }
    }


    private static class ListenerArgBundle {
        private final ImageProcessedListener listener;
        private final Handler handler;

        public ListenerArgBundle(@Nullable ImageProcessedListener listener, @Nullable Handler handler) {
            this.listener = listener;
            this.handler = handler;
        }
    }

    void sendImage2ProcessListener(@Nullable ImageProcessedListener listener, @Nullable Handler handler) {
        sendMessage(obtainMessage(MSG_SET_LISTENER, new ListenerArgBundle(listener, handler)));
    }

    void sendShutdown() {
        removeCallbacksAndMessages(null); // SHUTDOWN is top priority message, all others should be dropped
        sendMessage(obtainMessage(MSG_SHUTDOWN));
    }

    void sendLoadEffect(@NonNull String effectName) {
        sendMessage(obtainMessage(MSG_LOAD_EFFECT, effectName));
    }

    void sendUnloadEffect() {
        sendMessage(obtainMessage(MSG_UNLOAD_EFFECT));
    }

    void sendImage2Process(@NonNull Image image, @NonNull ImageOrientation orientation, @Nullable Integer outputImageFormat, long timestamp) {
        if (!hasMessages(MSG_CAMERA_IMAGE_PROCESS)) {
            sendMessage(obtainMessage(MSG_CAMERA_IMAGE_PROCESS, new ProcessAndroidCameraImageArgBundle(image, orientation, outputImageFormat, timestamp)));
        } else {
            image.close();
        }
    }

    void sendFullImage2Process(@NonNull FullImageData data, @Nullable ReleaseCallback releaseCallback, @Nullable Integer outputImageFormat, long timestamp, boolean isFrameSkipable) {
        sendMessage(obtainMessage(MSG_FULL_IMAGE_DATA, new ProcessFullImageDataArgBundle(data, releaseCallback, outputImageFormat, timestamp, isFrameSkipable)));
    }

    void sendCallJSMethod(@NonNull String method, @NonNull String parameter) {
        sendMessage(obtainMessage(MSG_CALL_JS_METHOD, new Pair<>(method, parameter)));
    }

    void sendEvalJs(@NonNull String script, @Nullable JsCallback resultCallback) {
        sendMessage(obtainMessage(MSG_EVAL_JS, new Pair<>(script, resultCallback)));
    }

    void sendSurfaceTexture(@Nullable SurfaceTexture surfaceTexture) {
        sendMessage(obtainMessage(MSG_SET_SURFACE, surfaceTexture));
    }

    void sendPlaybackPlay() {
        sendMessage(obtainMessage(MSG_PLAYBACK_PLAY));
    }

    void sendPlaybackPause() {
        sendMessage(obtainMessage(MSG_PLAYBACK_PAUSE));
    }

    void sendPlaybackStop() {
        sendMessage(obtainMessage(MSG_PLAYBACK_STOP));
    }

    @Override // runs on RenderThread
    public void handleMessage(@NonNull Message msg) {
        final OffscreenPlayerThread thread = getThread();
        if (thread != null) {
            switch (msg.what) {
                case MSG_SHUTDOWN:
                    thread.shutdown();
                    break;
                case MSG_LOAD_EFFECT:
                    thread.handleLoadEffect((String) msg.obj);
                    break;
                case MSG_UNLOAD_EFFECT:
                    thread.handleUnloadEffect();
                    break;
                case MSG_CAMERA_IMAGE_PROCESS:
                    final ProcessAndroidCameraImageArgBundle argsAndroidCameraProcess = (ProcessAndroidCameraImageArgBundle) msg.obj;
                    thread.handleImageProcess(argsAndroidCameraProcess.androidImage, argsAndroidCameraProcess.orientation, argsAndroidCameraProcess.outputImageFormat, argsAndroidCameraProcess.timestamp);
                    break;
                case MSG_FULL_IMAGE_DATA:
                    final ProcessFullImageDataArgBundle argProcess = (ProcessFullImageDataArgBundle) msg.obj;
                    if (hasMessages(MSG_FULL_IMAGE_DATA) && argProcess.isFrameSkipable) {
                        // Drop this frame because we already has better
                        final ReleaseCallback releaseCallback = argProcess.releaseCallback;
                        if (releaseCallback != null) {
                            releaseCallback.onRelease();
                        }
                    } else {
                        thread.handleFullImageData(argProcess.data, argProcess.releaseCallback, argProcess.outputImageFormat, argProcess.timestamp);
                    }
                    break;
                case MSG_SET_LISTENER:
                    final ListenerArgBundle argsListener = (ListenerArgBundle) msg.obj;
                    thread.handleSetListener(argsListener.listener, argsListener.handler);
                    break;
                case MSG_CALL_JS_METHOD:
                    @SuppressWarnings("unchecked")
                    final Pair<String, String> argsPair = (Pair<String, String>) msg.obj;
                    thread.handleCallJsMethod(argsPair.first, argsPair.second);
                    break;
                case MSG_EVAL_JS:
                    @SuppressWarnings("unchecked")
                    final Pair<String, JsCallback> argsPairJs = (Pair<String, JsCallback>) msg.obj;
                    thread.handleEvalJs(argsPairJs.first, argsPairJs.second);
                    break;
                case MSG_SET_SURFACE:
                    final SurfaceTexture surfaceTexture = (SurfaceTexture) msg.obj;
                    thread.handleSetSurface(surfaceTexture);
                    break;
                case MSG_PLAYBACK_PLAY:
                    thread.handlePlaybackPlay();
                    break;
                case MSG_PLAYBACK_PAUSE:
                    thread.handlePlaybackPause();
                    break;
                case MSG_PLAYBACK_STOP:
                    thread.handlePlaybackStop();
                    break;
                default:
                    throw new RuntimeException("unknown message " + msg.what);
            }
        } else {
            Logger.w("No render thread");
        }
    }
}
