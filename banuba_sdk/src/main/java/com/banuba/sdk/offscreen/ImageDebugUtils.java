// Developed by Banuba Development
// http://www.banuba.com
package com.banuba.sdk.offscreen;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

public final class ImageDebugUtils {
    private ImageDebugUtils() {
    }


    public static void saveImageDetailed(@NonNull Context context, int format, @NonNull ByteBuffer buffer, int w, int h, int rowStride, @NonNull String suffix, int frameNum, @Nullable ImageOrientation orientation) {
        final String frame = String.format(Locale.ENGLISH, "%04d", frameNum);

        final Map<String, String> map = orientation != null ? orientation.getParams() : new LinkedHashMap<>();

        saveImageDetailed(context, format, buffer, w, h, rowStride, frame + suffix, map);
    }

    public static void saveImageDetailed(@NonNull Context context, int format, @NonNull ByteBuffer buffer, int w, int h, int rowStride, @NonNull String filename, @Nullable Map<String, String> map) {
        final File file = new File(context.getExternalFilesDir(null), filename);

        final Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        if (format == android.graphics.PixelFormat.RGBA_8888) {
            buffer.rewind();
            bitmap.copyPixelsFromBuffer(buffer);
        } else if (format == ImageFormat.YUV_420_888) {
            final IntBuffer pixels = ByteBuffer.allocateDirect(w * h * 4).order(ByteOrder.nativeOrder()).asIntBuffer();
            for (int y = 0; y < h; y++) {
                buffer.position(rowStride * y);
                for (int x = 0; x < w; x++) {
                    final int value = buffer.get() & 0xFF;
                    pixels.position(y * w + x);
                    pixels.put(Color.argb(255, value, value, value));
                }
            }
            pixels.rewind();
            bitmap.copyPixelsFromBuffer(pixels);
        }

        if (map != null) {
            final Canvas canvas = new Canvas(bitmap);
            final Paint paint = new Paint();
            paint.setColor(Color.RED);
            paint.setTextSize(50);

            int i = 0;
            for (Map.Entry<String, String> entry : map.entrySet()) {
                canvas.drawText(entry.getKey() + " = " + entry.getValue(), 50, 100 + (i++) * 50, paint);
            }
        }

        try (FileOutputStream out = new FileOutputStream(file)) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("ANDREY9", " Save E = " + e.getMessage());
        }
        bitmap.recycle();
    }
}
