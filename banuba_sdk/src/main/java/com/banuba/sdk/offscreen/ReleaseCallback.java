package com.banuba.sdk.offscreen;

import androidx.annotation.Keep;

@Keep
public interface ReleaseCallback {
    void onRelease();
}
