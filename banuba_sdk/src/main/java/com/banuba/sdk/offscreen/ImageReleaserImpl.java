package com.banuba.sdk.offscreen;

import androidx.annotation.NonNull;

import com.banuba.sdk.internal.utils.Logger;
import com.banuba.sdk.utils.ImageReleaser;

import java.util.concurrent.atomic.AtomicInteger;


/**
 * ImageReleaserImpl class keep deleter for the frame and allowing it to be
 * released after destroying pointers to planes on the cpp side
 */

public final class ImageReleaserImpl implements ImageReleaser {
    private ReleaseCallback mReleaseCallback = null;
    private final AtomicInteger mRefCount = new AtomicInteger(0);

    public ImageReleaserImpl(@NonNull ReleaseCallback releaseCallback) {
        mReleaseCallback = releaseCallback;
    }

    @Override
    public void addRef() {
        mRefCount.incrementAndGet();
    }

    @Override
    public void addRefCount(int number) {
        assert number > 0 : "Trying to increase the number of references by a negative value";
        mRefCount.addAndGet(number);
    }

    @Override
    public void release() {
        if (mRefCount.decrementAndGet() == 0) {
            mReleaseCallback.onRelease();
            mReleaseCallback = null;
        } else if (mRefCount.get() < 0) {
            Logger.w("All planes have already been deallocate, or the allocatePlane() method has not been called");
        }
    }

    public boolean isClosed() {
        return mReleaseCallback == null;
    }
}

/** @} */ // endgroup
