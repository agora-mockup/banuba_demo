package com.banuba.sdk.offscreen;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;

import java.nio.ByteBuffer;

/**
 * Buffer Allocator interface that can be used for memory reusing without new allocation.
 */
@Keep
public interface BufferAllocator {
    /**
     * New buffer allocate.
     *
     * @param minimumCapacity minimum buffer capacity.
     * @return allocated {@link ByteBuffer}.
     */
    @NonNull
    ByteBuffer allocateBuffer(int minimumCapacity);
}
