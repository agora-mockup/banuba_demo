package com.banuba.sdk.offscreen;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.banuba.sdk.offscreen.ImageTransform.FLIP_X;
import static com.banuba.sdk.offscreen.ImageTransform.FLIP_Y;
import static com.banuba.sdk.offscreen.YUVConverterMod.CONV_MODE_STEP_NEG_X;
import static com.banuba.sdk.offscreen.YUVConverterMod.CONV_MODE_STEP_NEG_Y;
import static com.banuba.sdk.offscreen.YUVConverterMod.CONV_MODE_STEP_X;
import static com.banuba.sdk.offscreen.YUVConverterMod.CONV_MODE_STEP_Y;

public final class ImageOrientation {
    private static final ImageTransform[] TRANSFORMS_CAMERA;
    private static final ImageTransform[] TRANSFORMS_ROTATIONS;

    static {
        TRANSFORMS_CAMERA = new ImageTransform[] {
            new ImageTransform(90, FLIP_X, CONV_MODE_STEP_NEG_Y, 90, FLIP_X), // 0 = Surface.ROTATION_0, PORTRAIT
            new ImageTransform(0, FLIP_Y, CONV_MODE_STEP_X, 0, FLIP_Y),       // 1 = Surface.ROTATION_90, LANDSCAPE
            new ImageTransform(90, FLIP_Y, CONV_MODE_STEP_Y, 90, FLIP_Y),     // 2 = Surface.ROTATION_180, REVERSE_PORTRAIT
            new ImageTransform(0, FLIP_X, CONV_MODE_STEP_NEG_X, 0, FLIP_X),   // 3 = Surface.ROTATION_270, REVERSE_LANDSCAPE
        };

        TRANSFORMS_ROTATIONS = new ImageTransform[] {
            new ImageTransform(0, FLIP_Y, CONV_MODE_STEP_X, 0, FLIP_Y),       // 0
            new ImageTransform(90, FLIP_X, CONV_MODE_STEP_NEG_Y, 90, FLIP_X), // 90
            new ImageTransform(0, FLIP_X, CONV_MODE_STEP_NEG_X, 0, FLIP_X),   // 180
            new ImageTransform(90, FLIP_Y, CONV_MODE_STEP_Y, 90, FLIP_Y),     // 270 (90 clockwise)
        };
    }

    private final ImageTransform[] mTransforms;

    private final boolean mCameraMode;

    private final int mRotationIndex;
    private final int mRotationAngle;

    private final int mCameraSensorOrientation;
    private final int mAccelerometerDeviceOrientation;

    private final int mImageOrientationAngle;
    private final boolean mIsRequireMirroring;

    private ImageOrientation(@NonNull ImageTransform[] transforms, int sensorOrientation, int surfaceRotation, int deviceOrientation) {
        mRotationIndex = surfaceRotation;
        mRotationAngle = surfaceRotation * 90;
        mCameraMode = true;
        mTransforms = transforms;
        mCameraSensorOrientation = sensorOrientation;
        mAccelerometerDeviceOrientation = deviceOrientation;
        mImageOrientationAngle = sensorOrientation;
        mIsRequireMirroring = false;
    }

    private ImageOrientation(@NonNull ImageTransform[] transforms, int imageOrientationAngle, int deviceOrientationAngle, int surfaceRotationIndex, boolean isRequireMirroring) {
        mTransforms = transforms;
        mImageOrientationAngle = imageOrientationAngle;
        mRotationIndex = surfaceRotationIndex;
        mRotationAngle = surfaceRotationIndex * 90;
        mAccelerometerDeviceOrientation = deviceOrientationAngle;
        mIsRequireMirroring = isRequireMirroring;
        mCameraMode = false;
        mCameraSensorOrientation = 0;
    }

    private ImageOrientation(@NonNull ImageTransform[] transforms, int frameRotationAngle) {
        mTransforms = transforms;
        mRotationAngle = frameRotationAngle;
        mRotationIndex = frameRotationAngle / 90;
        mCameraMode = false;
        mCameraSensorOrientation = 0;
        mAccelerometerDeviceOrientation = 0;
        mImageOrientationAngle = 0;
        mIsRequireMirroring = false;
    }

    @Keep
    @NonNull
    public static ImageOrientation getForCamera(int sensorOrientation, int surfaceRotation, int deviceOrientationAngle) {
        return new ImageOrientation(TRANSFORMS_CAMERA, sensorOrientation, surfaceRotation, deviceOrientationAngle);
    }

    @Keep
    @NonNull
    public static ImageOrientation getForCamera(int imageOrientationAngle, int deviceOrientationAngle, int surfaceRotationIndex, boolean isRequireMirroring) {
        return new ImageOrientation(TRANSFORMS_CAMERA, imageOrientationAngle, deviceOrientationAngle, surfaceRotationIndex, isRequireMirroring);
    }

    @Keep
    public static ImageOrientation getForBufferFrame(int frameRotationAngle) {
        return new ImageOrientation(TRANSFORMS_ROTATIONS, frameRotationAngle);
    }

    @Keep
    @NonNull
    public float[] getDefaultDrawMatrix(int displaySurfaceRotation) {
        return mTransforms[displaySurfaceRotation].getScreenMatrix();
    }

    @NonNull
    float[] getConvertMatrix() {
        return mTransforms[mRotationIndex].getConvertMatrix();
    }

    int getCameraSensorOrientation() {
        return mCameraSensorOrientation;
    }

    @Keep
    public int getRotationIndex() {
        return mRotationIndex;
    }

    @Keep
    public int getRotationAngle() {
        return mRotationAngle;
    }

    @Keep
    public int getImageOrientationAngle() {
        return mImageOrientationAngle;
    }

    @Keep
    public boolean isRequireMirroring() {
        return mIsRequireMirroring;
    }

    @Keep
    public int getAccelerometerDeviceOrientation() {
        return mAccelerometerDeviceOrientation;
    }

    int getConverterMode() {
        return mTransforms[mRotationIndex].getConverterMode();
    }

    @NonNull
    public Map<String, String> getParams() {
        final Map<String, String> map = new LinkedHashMap<>();

        if (mCameraMode) {
            map.put("Mode", "Camera");
            map.put("DisplaySurfaceRotation", mRotationIndex + " (" + mRotationIndex * 90 + " deg)");
            map.put("CameraSensorOrientation", Integer.toString(mCameraSensorOrientation));
            map.put("DeviceAccelerometerOrientation", Integer.toString(mAccelerometerDeviceOrientation));
        } else {
            map.put("Mode", "FrameAngle");
            map.put("FrameRotationAngle", Integer.toString(mRotationAngle));
        }
        return map;
    }
}
