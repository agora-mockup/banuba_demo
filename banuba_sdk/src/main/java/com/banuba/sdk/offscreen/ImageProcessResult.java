package com.banuba.sdk.offscreen;

import android.graphics.ImageFormat;
import android.graphics.PixelFormat;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;

import java.nio.ByteBuffer;

@Keep
public class ImageProcessResult {
    private final int format;
    private final long timestamp;
    private final ImageOrientation orientation;
    private final ByteBuffer buffer;

    private final int planeCount;
    private final int[] rowStrides;
    private final int[] pixelStrides;
    private final int[] planesOffsets;
    private final int[] planesWidths;
    private final int[] planesHeights;

    ImageProcessResult(int width, int height, int format, long timestamp, @NonNull ImageOrientation orientation, @NonNull ByteBuffer buffer) {
        this.format = format;
        this.timestamp = timestamp;
        this.orientation = orientation;
        this.buffer = buffer;

        if (format == ImageFormat.YUV_420_888) {
            this.planeCount = 3;
            this.planesHeights = new int[] {height, height / 2, height / 2};
            this.planesWidths = new int[] {width, width / 2, width / 2};
            this.pixelStrides = new int[] {1, 1, 1};
            int stride = (width + 7) & ~7;
            this.rowStrides = new int[] {stride, stride, stride};
            this.planesOffsets = new int[] {0, stride * height, stride * height + stride * height / 2};
        } else if (format == PixelFormat.RGBA_8888) {
            this.planeCount = 1;
            this.rowStrides = new int[] {width};
            this.pixelStrides = new int[] {4};
            this.planesOffsets = new int[] {0};
            this.planesHeights = new int[] {height};
            this.planesWidths = new int[] {width};
        } else {
            throw new RuntimeException("Unknown Image format = " + format);
        }
    }

    public int getPlaneCount() {
        return planeCount;
    }

    public int getWidthOfPlane(int planeNum) {
        return planesWidths[planeNum];
    }

    public int getHeightOfPlane(int planeNum) {
        return planesHeights[planeNum];
    }

    public int getBytesPerRowOfPlane(int planeNum) {
        return rowStrides[planeNum];
    }

    public int getOffsetOfPlane(int planeNum) {
        return planesOffsets[planeNum];
    }

    public int getWidth() {
        return planesWidths[0];
    }

    public int getHeight() {
        return planesHeights[0];
    }

    public int getBytesPerRow() {
        return rowStrides[0];
    }

    @NonNull
    public int[] getPixelStrides() {
        return pixelStrides;
    }

    public int getPixelStride(int planeNum) {
        return pixelStrides[planeNum];
    }

    @NonNull
    public int[] getRowStrides() {
        return rowStrides;
    }

    public int getRowStride(int planeNum) {
        return rowStrides[planeNum];
    }

    public int getFormat() {
        return format;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @NonNull
    public ImageOrientation getOrientation() {
        return orientation;
    }

    @NonNull
    public ByteBuffer getBuffer() {
        buffer.position(0);
        buffer.limit(buffer.capacity());
        return buffer;
    }

    @NonNull
    public ByteBuffer getPlaneBuffer(int planeNum) {
        this.buffer.position(this.getOffsetOfPlane(planeNum));
        this.buffer.limit(this.getOffsetOfPlane(planeNum) + this.getBytesPerRowOfPlane(planeNum) * this.getHeightOfPlane(planeNum));
        return this.buffer.slice();
    }
}
