package com.banuba.sdk.offscreen;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Encapsulates info about Offscreen Effect Player ({@link OffscreenEffectPlayer}) Configuration (Simple version).
 */
@Keep
public final class OffscreenSimpleConfig {
    @Nullable
    private final BufferAllocator bufferAllocator;
    private final boolean debugSaveFrames;
    private final int debugSaveFramesDivider;

    private OffscreenSimpleConfig(Builder builder) {
        this.bufferAllocator = builder.bufferAllocator;
        this.debugSaveFrames = builder.debugSaveFrames;
        this.debugSaveFramesDivider = builder.debugSaveFramesDivider;
    }

    /**
     * Return buffer allocator.
     *
     * @return {@link BufferAllocator}.
     */
    @Nullable
    public BufferAllocator getBufferAllocator() {
        return bufferAllocator;
    }

    /**
     *
     * Is frame need to be saved.
     *
     * @param frameNum frame number.
     * @return saving status.
     */
    public boolean isSaveFrame(int frameNum) {
        if (debugSaveFrames) {
            return frameNum % debugSaveFramesDivider == 0;
        }
        return false;
    }

    /**
     * Create new {@link Builder} instance.
     *
     * @param allocator {@link BufferAllocator} instance.
     * @return {@link Builder} instance.
     */
    public static Builder newBuilder(@Nullable BufferAllocator allocator) {
        return new Builder(allocator);
    }

    @Keep
    public static class Builder {
        @Nullable
        private final BufferAllocator bufferAllocator;
        private boolean debugSaveFrames = false;
        private int debugSaveFramesDivider = 100;

        private Builder(@Nullable BufferAllocator allocator) {
            this.bufferAllocator = allocator;
        }

        /**
         * Set debug save frames flag.
         *
         * @param debugSaveFrames debug save frames flag value.
         * @return {@link Builder} instance.
         */
        @NonNull
        public Builder setDebugSaveFrames(boolean debugSaveFrames) {
            this.debugSaveFrames = debugSaveFrames;
            return this;
        }

        /**
         * Set debug save frames divider.
         *
         * @param debugSaveFramesDivider debug save frames divider value.
         * @return {@link Builder} instance.
         */
        @NonNull
        public Builder setDebugSaveFramesDivider(int debugSaveFramesDivider) {
            this.debugSaveFramesDivider = debugSaveFramesDivider;
            return this;
        }

        /**
         * Build the {@link OffscreenSimpleConfig} object.
         *
         * @return {@link OffscreenSimpleConfig} object.
         */
        @NonNull
        public OffscreenSimpleConfig build() {
            return new OffscreenSimpleConfig(this);
        }
    }
}
