package com.banuba.sdk.offscreen;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.media.Image;
import android.opengl.GLES20;
import android.os.Handler;
import android.util.Size;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.banuba.sdk.Recycler;
import com.banuba.sdk.effect_player.CameraOrientation;
import com.banuba.sdk.effect_player.ConsistencyMode;
import com.banuba.sdk.effect_player.Effect;
import com.banuba.sdk.effect_player.EffectManager;
import com.banuba.sdk.effect_player.EffectPlayer;
import com.banuba.sdk.effect_player.EffectPlayerPlaybackState;
import com.banuba.sdk.effect_player.JsCallback;
import com.banuba.sdk.internal.BaseWorkThread;
import com.banuba.sdk.internal.gl.EglCore;
import com.banuba.sdk.internal.gl.GLFullRectTexture;
import com.banuba.sdk.internal.gl.OffscreenSurface;
import com.banuba.sdk.internal.gl.RenderBuffer;
import com.banuba.sdk.internal.gl.WindowSurface;
import com.banuba.sdk.internal.utils.OrientationHelper;
import com.banuba.sdk.manager.BanubaSdkManager;
import com.banuba.sdk.types.FullImageData;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.Objects;

public class OffscreenPlayerThread extends BaseWorkThread<OffscreenPlayerHandler> {
    // clang-format off
    private static final float[] MATRIX_V_FLIP = {
            1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, -1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 1.0f
    };
    // clang-format on

    private final Context mContext;
    private final int mVerticalPictureWidth;
    private final int mVerticalPictureHeight;
    @NonNull
    private final OffscreenSimpleConfig mConfig;
    private final int mMinDimension;
    private final int mMaxDimension;
    @Nullable
    private final BufferAllocator mAllocator;

    private final SingleRenderBufferAllocator mPlayerRenderBufferAllocator = new SingleRenderBufferAllocator();
    private final SingleRenderBufferAllocator mConvertRenderBufferAllocator = new SingleRenderBufferAllocator();

    private EglCore mEglCore;
    private EffectPlayer mEffectPlayer;
    private ImageProcessedListener mProcessListener;
    private Handler mProcessHandler;
    private Effect mEffect;
    private YUVConverterMod mYUVConverter;

    private int mSavedImageOrientation = -1;

    private int mFrameNumber;

    private int mSavedEffectWidth;
    private int mSavedEffectHeight;

    private OffscreenSurface mOffscreenSurface;

    private GLFullRectTexture mTextureDrawer;

    private WindowSurface mExternalSurface;

    public OffscreenPlayerThread(@NonNull Context context, @NonNull OffscreenEffectPlayerConfig config, @NonNull String key) {
        super("OffscreenEffectPlayerThread");
        mContext = context;
        mConfig = config.getSimpleConfig();
        mAllocator = config.getSimpleConfig().getBufferAllocator();
        mVerticalPictureWidth = config.getVerticalPictureWidth();
        mVerticalPictureHeight = config.getVerticalPictureHeight();
        mMinDimension = config.getMinDimension();
        mMaxDimension = config.getMaxDimension();

        BanubaSdkManager.initialize(mContext, key);
        mEffectPlayer = Objects.requireNonNull(EffectPlayer.create(config.getEffectPlayerConfig()));

        // NOTE: Relevant setting for the best performance in Android
        mEffectPlayer.setRenderConsistencyMode(ConsistencyMode.ASYNCHRONOUS_CONSISTENT_WHEN_EFFECT_LOADED);
    }

    public OffscreenPlayerThread(
        @NonNull Context context, @NonNull EffectPlayer player, @NonNull Size size, @NonNull OffscreenSimpleConfig config) {
        super("OffscreenEffectPlayerThread");
        mContext = context;
        mEffectPlayer = player;

        // NOTE: Relevant setting for the best performance in Android
        mEffectPlayer.setRenderConsistencyMode(ConsistencyMode.ASYNCHRONOUS_CONSISTENT_WHEN_EFFECT_LOADED);

        mConfig = config;
        mAllocator = config.getBufferAllocator();

        int minWidth = Math.min(size.getWidth(), size.getHeight());
        int maxHeight = Math.max(size.getWidth(), size.getHeight());
        mVerticalPictureWidth = minWidth;
        mVerticalPictureHeight = maxHeight;
        mMinDimension = minWidth;
        mMaxDimension = maxHeight;
    }

    @NonNull
    @Override
    protected OffscreenPlayerHandler constructHandler() {
        return new OffscreenPlayerHandler(this);
    }


    protected void preRunInit() {
        mEglCore = new EglCore(null, 0x2); // 0x2 = ES_3.0+

        mOffscreenSurface = new OffscreenSurface(mEglCore, 16, 16);
        mOffscreenSurface.makeCurrent();

        mEffectPlayer.surfaceCreated(mVerticalPictureWidth, mVerticalPictureHeight);
        mEffectPlayer.playbackPlay();

        mTextureDrawer = new GLFullRectTexture(false);
    }


    protected void postRunClear() {
        if (mEffectPlayer != null) {
            mEffectPlayer.surfaceDestroyed();
        }
        mEffectPlayer = Recycler.recycle(mEffectPlayer);
        mConvertRenderBufferAllocator.clear();
        mPlayerRenderBufferAllocator.clear();
        if (mYUVConverter != null) {
            mYUVConverter.clear();
        }
        mEglCore.makeNothingCurrent();
        mEglCore.release();
    }

    public void handleLoadEffect(@NonNull String effectName) {
        if (mEffectPlayer != null) {
            final EffectManager manager = mEffectPlayer.effectManager();
            if (manager != null) {
                mEffect = manager.loadAsync(effectName);
            }
        }
    }

    public void handleImageProcess(@NonNull Image image, @NonNull ImageOrientation imageOrientation, @Nullable Integer outputImageFormat, long timestamp) {
        final EffectPlayer player = mEffectPlayer;
        if (player == null) {
            return;
        }

        final int inputPictureWidth = image.getWidth();
        final int inputPictureHeight = image.getHeight();

        int epPictureWidth;
        int epPictureHeight;

        if (imageOrientation.getImageOrientationAngle() % 180 == 0) {
            epPictureWidth = mMaxDimension;
            epPictureHeight = mMinDimension;
        } else {
            epPictureWidth = mMinDimension;
            epPictureHeight = mMaxDimension;
        }

        if (mSavedImageOrientation != imageOrientation.getImageOrientationAngle()) {
            player.surfaceChanged(epPictureWidth, epPictureHeight);
            final EffectManager manager = player.effectManager();
            if (manager != null) {
                manager.setEffectSize(epPictureWidth, epPictureHeight);
            }
            player.playbackPlay();
            mSavedImageOrientation = imageOrientation.getImageOrientationAngle();
        }

        final FullImageData.Orientation sdkOrientation = OrientationHelper.getOrientation(
            imageOrientation.getImageOrientationAngle(),
            imageOrientation.getAccelerometerDeviceOrientation(),
            imageOrientation.isRequireMirroring());

        final int format = image.getFormat();
        final Image.Plane[] planes = image.getPlanes();
        final FullImageData sdkImage = new FullImageData(
            new Size(inputPictureWidth, inputPictureHeight),
            planes[0].getBuffer(),
            planes[1].getBuffer(),
            planes[2].getBuffer(),
            planes[0].getRowStride(),
            planes[1].getRowStride(),
            planes[2].getRowStride(),
            planes[0].getPixelStride(),
            planes[1].getPixelStride(),
            planes[2].getPixelStride(),
            sdkOrientation);

        player.pushFrameWithNumber(sdkImage, timestamp);

        image.close();

        processFrame(player, epPictureWidth, epPictureHeight, inputPictureWidth, inputPictureHeight, format, outputImageFormat, timestamp, imageOrientation);

        mFrameNumber++;
    }

    // Please provide valid timestamp of the frame in NanoSeconds
    public void handleFullImageData(@NonNull FullImageData data, @Nullable ReleaseCallback callback, @Nullable Integer outputImageFormat, long timestamp) {
        final EffectPlayer player = mEffectPlayer;
        if (player != null) {
            final boolean swap = data.getOrientation().getCameraOrientation() == CameraOrientation.DEG_90 || data.getOrientation().getCameraOrientation() == CameraOrientation.DEG_270;

            final int angle = getAngleValue(data.getOrientation().getCameraOrientation());

            final Size size = data.getSize();
            final int renderWidth = swap ? size.getHeight() : size.getWidth();
            final int renderHeight = swap ? size.getWidth() : size.getHeight();

            if (mSavedEffectWidth != renderWidth || mSavedEffectHeight != renderHeight) {
                final EffectManager manager = player.effectManager();
                if (manager != null) {
                    manager.setEffectSize(renderWidth, renderHeight);
                }
                mSavedEffectWidth = renderWidth;
                mSavedEffectHeight = renderHeight;
                player.surfaceChanged(renderWidth, renderHeight);
                player.playbackPlay();
            }

            if (callback != null) {
                ImageReleaserImpl imageReleaser = new ImageReleaserImpl(callback);
                data.setImageReleaser(imageReleaser);
            }

            player.pushFrameWithNumber(data, timestamp);

            final ImageOrientation imageOrientation = ImageOrientation.getForBufferFrame(angle);
            processFrame(player, renderWidth, renderHeight, size.getWidth(), size.getHeight(), data.getPixelFormat(), outputImageFormat, timestamp, imageOrientation);

            mFrameNumber++;
        }
    }

    private void processFrame(@NonNull EffectPlayer player, int renderWidth, int renderHeight, int resultWidth, int resultHeight, int inputFormat, @Nullable Integer outputFormat, long timestamp, @NonNull ImageOrientation imageOrientation) {
        mOffscreenSurface.makeCurrent();
        final RenderBuffer renderBuffer = mPlayerRenderBufferAllocator.getRenderBuffer(renderWidth, renderHeight);
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, renderBuffer.getFrameBufferId());
        GLES20.glViewport(0, 0, renderWidth, renderHeight);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

        long drawResult;
        do {
            drawResult = player.draw();
        } while (drawResult < 0);
        debugSaveEffectPlayerFrame(imageOrientation, renderWidth, renderHeight);

        if (mExternalSurface != null) {
            mExternalSurface.makeCurrent();
            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
            final int maxDim = Math.max(renderWidth, renderHeight);
            GLES20.glViewport(0, 0, maxDim, maxDim);
            GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
            mTextureDrawer.draw(renderBuffer.getTextureId(), MATRIX_V_FLIP);
            final long outTimeStamp = (timestamp & ~0x3) | imageOrientation.getRotationIndex();
            mExternalSurface.setPresentationTime(outTimeStamp);
            mExternalSurface.swapBuffers();
        }

        final Handler handler = mProcessHandler;
        final ImageProcessedListener listener = mProcessListener;
        if (handler != null && listener != null) {
            final int format = outputFormat != null ? outputFormat : inputFormat;
            if (format == ImageFormat.YUV_420_888) {
                final int size = 2 * resultWidth * resultHeight;
                final ByteBuffer buffer = mAllocator != null ? mAllocator.allocateBuffer(size) : ByteBuffer.allocateDirect(size);
                final YUVConverterMod converter = getYUVConverter(resultWidth, resultHeight);
                converter.convert(renderBuffer.getTextureId(), buffer, imageOrientation.getConvertMatrix(), imageOrientation.getConverterMode());
                debugSaveResultFrame(imageOrientation, format, resultWidth, resultHeight, buffer);
                handler.post(() -> listener.onImageProcessed(new ImageProcessResult(resultWidth, resultHeight, format, timestamp, imageOrientation, buffer)));

            } else if (format == PixelFormat.RGBA_8888) {
                final int size = 4 * resultWidth * resultHeight;
                final ByteBuffer buffer = mAllocator != null ? mAllocator.allocateBuffer(size) : ByteBuffer.allocateDirect(size);
                final RenderBuffer convert = mConvertRenderBufferAllocator.getRenderBuffer(resultWidth, resultHeight);
                GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, convert.getFrameBufferId());
                GLES20.glViewport(0, 0, resultWidth, resultHeight);
                GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
                mTextureDrawer.draw(renderBuffer.getTextureId(), imageOrientation.getConvertMatrix());
                debugSaveResultFrame(imageOrientation, format, resultWidth, resultHeight, buffer);
                GLES20.glReadPixels(0, 0, resultWidth, resultHeight, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, buffer);
                buffer.rewind();
                handler.post(() -> listener.onImageProcessed(new ImageProcessResult(resultWidth, resultHeight, format, timestamp, imageOrientation, buffer)));
            }
        }

        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
    }

    private int getAngleValue(@NonNull CameraOrientation cameraOrientation) {
        switch (cameraOrientation) {
            case DEG_90:
                return 90;
            case DEG_180:
                return 180;
            case DEG_270:
                return 270;
            default:
                return 0;
        }
    }

    private void debugSaveResultFrame(@NonNull ImageOrientation orientation, int format, int inputPictureWidth, int inputPictureHeight, ByteBuffer buffer) {
        if (mConfig.isSaveFrame(mFrameNumber)) {
            ImageDebugUtils.saveImageDetailed(mContext, format, buffer, inputPictureWidth, inputPictureHeight, inputPictureWidth, "result.jpg", mFrameNumber, orientation);
        }
    }

    private void debugSaveEffectPlayerFrame(@NonNull ImageOrientation orientation, int epPictureWidth, int epPictureHeight) {
        if (mConfig.isSaveFrame(mFrameNumber)) {
            final ByteBuffer bufferSwapped = ByteBuffer.allocateDirect(epPictureWidth * epPictureHeight * 4);
            final ByteBuffer buffer = ByteBuffer.allocateDirect(epPictureWidth * epPictureHeight * 4);

            GLES20.glReadPixels(0, 0, epPictureWidth, epPictureHeight, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, bufferSwapped);

            final IntBuffer intBufferSwapped = bufferSwapped.order(ByteOrder.nativeOrder()).asIntBuffer();
            final IntBuffer intBuffer = buffer.order(ByteOrder.nativeOrder()).asIntBuffer();

            for (int y = epPictureHeight - 1; y >= 0; y--) {
                intBufferSwapped.position(epPictureWidth * y);
                for (int x = 0; x < epPictureWidth; x++) {
                    intBuffer.put(intBufferSwapped.get());
                }
            }
            buffer.rewind();
            ImageDebugUtils.saveImageDetailed(mContext, PixelFormat.RGBA_8888, buffer, epPictureWidth, epPictureHeight, epPictureWidth, "player.jpg", mFrameNumber, orientation);
        }
    }

    @NonNull
    private YUVConverterMod getYUVConverter(int width, int height) {
        if (mYUVConverter == null) {
            mYUVConverter = new YUVConverterMod(width, height);
        } else {
            if (mYUVConverter.getWidth() != width || mYUVConverter.getHeight() != height) {
                mYUVConverter.clear();
                mYUVConverter = new YUVConverterMod(width, height);
            }
        }
        return mYUVConverter;
    }

    public void handleUnloadEffect() {
        if (mEffectPlayer != null && mEffect != null) {
            final EffectManager manager = mEffectPlayer.effectManager();
            if (manager != null) {
                manager.unload(mEffect);
            }
        }
    }

    public void handleCallJsMethod(@NonNull String method, @NonNull String parameter) {
        final Effect effect = mEffect;
        if (effect != null) {
            effect.callJsMethod(method, parameter);
        }
    }

    public void handleEvalJs(@NonNull String script, @Nullable JsCallback resultCallback) {
        final Effect effect = mEffect;
        if (effect != null) {
            effect.evalJs(script, resultCallback);
        }
    }

    public void handleSetListener(@Nullable ImageProcessedListener listener, @Nullable Handler handler) {
        mProcessListener = listener;
        mProcessHandler = handler;
    }

    public void handleSetSurface(@Nullable SurfaceTexture surfaceTexture) {
        if (surfaceTexture != null) {
            surfaceTexture.setDefaultBufferSize(mMaxDimension, mMaxDimension);
            mExternalSurface = new WindowSurface(mEglCore, surfaceTexture);
        } else {
            mExternalSurface = null;
        }
    }

    public void handlePlaybackPlay() {
        mEffectPlayer.playbackPlay();
    }

    public void handlePlaybackPause() {
        mEffectPlayer.playbackPause();
    }

    public void handlePlaybackStop() {
        mEffectPlayer.playbackStop();
    }
}
