package com.banuba.sdk.internal.camera;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.os.Build;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;

import java.util.Locale;

public class CameraFormatHelper {
    public static int getPreferablePreviewFormat(@NonNull Context context) {
        // We know that capturing of RGBA is faster than YUV, but only few devices support RGBA
        // Support of RGBA is not reported via getSupportedFormats, so our way is
        // Try RGBA, fallback to YUV. Store working format in Shared Preferences after first try.
        // Retry format after Android and Application update

        // NOTE: None of known Samsung devices do not open camera with RGBA.
        // Use YUV_420_888 for all Samsung devices.
        if (isBrand("samsung")) {
            return ImageFormat.YUV_420_888;
        } else {
            final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            final int format = preferences.getInt(getPreviewFormatKey(context), PixelFormat.RGBA_8888);
            return (format == PixelFormat.RGBA_8888 || format == ImageFormat.YUV_420_888) ? format : PixelFormat.RGBA_8888;
        }
    }

    public static void savePreferablePreviewFormat(@NonNull Context context, int format) {
        final String key = getPreviewFormatKey(context);
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, format);
        editor.apply();
    }

    private static boolean isBrand(@NonNull String template) {
        final String manufacturer = android.os.Build.MANUFACTURER;
        final String brand = android.os.Build.BRAND;

        final boolean isManufacturer = manufacturer != null && manufacturer.toLowerCase(Locale.ENGLISH).contains(template);
        final boolean isBrand = brand != null && brand.toLowerCase(Locale.ENGLISH).contains(template);

        return isManufacturer || isBrand;
    }


    @NonNull
    private static String getPreviewFormatKey(@NonNull Context context) {
        return "CAMERA.FORMAT." + Long.toHexString(getVersionCode(context)) + "." + Long.toHexString(Build.VERSION.SDK_INT);
    }

    private static long getVersionCode(@NonNull Context context) {
        long versionCode = 0;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                versionCode = pInfo.getLongVersionCode();
            } else {
                versionCode = pInfo.versionCode;
            }
        } catch (PackageManager.NameNotFoundException e) {
            // Do nothing
        }
        return versionCode;
    }

    @NonNull
    public static String decodePreviewFormat(int format) {
        switch (format) {
            case ImageFormat.YUV_420_888:
                return "ImageFormat.YUV_420_888";
            case PixelFormat.RGBA_8888:
                return "PixelFormatType.RGBA_8888";
        }
        return "UnknownFormat[" + format + "]";
    }
}
