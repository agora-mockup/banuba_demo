package com.banuba.sdk.internal.utils;

import android.content.Context;
import android.util.Log;
import android.view.OrientationEventListener;

import androidx.annotation.NonNull;

import com.banuba.sdk.effect_player.CameraOrientation;
import com.banuba.sdk.types.FullImageData;

import static com.banuba.sdk.types.FullImageData.Orientation;

/**
 * Orientation helper class.
 */
public final class OrientationHelper {
    private static final String TAG = "OrientationHelper";

    private static final int ORIENTATION_PORTRAIT_NORMAL = 0;
    private static final int ORIENTATION_LANDSCAPE_NORMAL = 90;
    private static final int ORIENTATION_PORTRAIT_INVERTED = 180;
    private static final int ORIENTATION_LANDSCAPE_INVERTED = 270;

    private static OrientationHelper instance;

    private static final OrientationCreator ORIENTATION_CREATOR = (imageAngle, faceAngle, requiredMirroring) -> {
        CameraOrientation cameraOrientation = CameraOrientation.DEG_0;
        switch (imageAngle) {
            case ORIENTATION_PORTRAIT_NORMAL:
                cameraOrientation = CameraOrientation.DEG_0;
                break;
            case ORIENTATION_LANDSCAPE_NORMAL:
                cameraOrientation = CameraOrientation.DEG_90;
                break;
            case ORIENTATION_PORTRAIT_INVERTED:
                cameraOrientation = CameraOrientation.DEG_180;
                break;
            case ORIENTATION_LANDSCAPE_INVERTED:
                cameraOrientation = CameraOrientation.DEG_270;
                break;
        }
        return new FullImageData.Orientation(cameraOrientation, requiredMirroring, faceAngle);
    };

    @NonNull
    private final OrientationEventListener mOrientationEventListener;

    private int mCurrentOrientationAngle = 0;

    private boolean isOrientationListenerStarted = false;

    private OrientationHelper(@NonNull Context context) {
        mOrientationEventListener = new OrientationEventListener(context) {
            @Override
            public void onOrientationChanged(int orientation) {
                if (orientation < 35 || 325 <= orientation) {
                    mCurrentOrientationAngle = ORIENTATION_PORTRAIT_NORMAL;
                } else if (235 <= orientation && orientation < 305) {
                    mCurrentOrientationAngle = ORIENTATION_LANDSCAPE_NORMAL;
                } else if (145 <= orientation && orientation < 215) {
                    mCurrentOrientationAngle = ORIENTATION_PORTRAIT_INVERTED;
                } else if (55 <= orientation && orientation < 125) {
                    mCurrentOrientationAngle = ORIENTATION_LANDSCAPE_INVERTED;
                }
            }
        };

        Log.i(TAG, "OrientationHelper was instantiated.");
    }

    public void startDeviceOrientationUpdates() {
        mOrientationEventListener.enable();
        isOrientationListenerStarted = true;

        Log.i(TAG, "OrientationEventListener was enabled.");
    }

    public void stopDeviceOrientationUpdates() {
        mOrientationEventListener.disable();
        isOrientationListenerStarted = false;

        Log.i(TAG, "OrientationEventListener was disabled.");
    }

    public int getDeviceOrientationAngle() {
        if (!isOrientationListenerStarted) {
            Log.w(TAG, "OrientationEventListener was not enabled.");
        }

        return mCurrentOrientationAngle;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();

        mOrientationEventListener.disable();

        Log.i(TAG, "OrientationHelper was destroyed.");
    }

    public static OrientationHelper getInstance(@NonNull Context context) {
        if (instance == null) {
            instance = new OrientationHelper(context);
        }
        return instance;
    }

    /**
     * Get face orientation angle value based on camera parameters.
     *
     * Image, device and camera orientation angles should be from list of [0, 90, 180, 270].
     * Image orientation angles match to the next head directions:
     * 0 - head-left direction;
     * 90 - head-down direction;
     * 180 - head-right direction;
     * 270 - head-top direction.
     * Device orientation angles match to the device main button direction next way:
     * 0 - button-down direction;
     * 90 - button-right direction;
     * 180 - button-top direction;
     * 270 - button-left direction.
     *
     * @param imageAngle image orientation angle value.
     * @param deviceAngle device orientation angle value.
     * @param cameraAngle camera orientation angle value.
     * @param frontCamera front camera flag value.
     * @param requireMirroring mirroring required flag value.
     *
     * @return face orientation angle value, should be one from the list of [0, 90, 180, 270].
     */
    public static int getFaceOrientationByCamera(final int imageAngle, final int deviceAngle, final int cameraAngle, final boolean frontCamera, final boolean requireMirroring) {
        int diff = 0;
        if (frontCamera) {
            diff = cameraAngle - imageAngle + deviceAngle;
        } else {
            diff = 360 - deviceAngle;
            diff = cameraAngle - imageAngle + diff;
        }
        diff = diff % 360;
        if (!requireMirroring && diff % 180 != 0) {
            return -1 * diff;
        }
        return diff;
    }

    /**
     * Get {@link FullImageData.Orientation} based on camera parameters.
     *
     * Image, device and camera orientation angles should be from list of [0, 90, 180, 270].
     * Image orientation angles match to the next head directions:
     * 0 - head-left direction;
     * 90 - head-down direction;
     * 180 - head-right direction;
     * 270 - head-top direction.
     * Device orientation angles match to the device main button direction next way:
     * 0 - button-down direction;
     * 90 - button-right direction;
     * 180 - button-top direction;
     * 270 - button-left direction.
     *
     * @param imageAngle image orientation angle value.
     * @param deviceAngle device orientation angle value.
     * @param cameraAngle camera orientation angle value.
     * @param frontCamera front camera flag value.
     * @param requireMirroring mirroring required flag value.
     *
     * @return {@link FullImageData.Orientation}
     */
    public static Orientation getOrientationByCamera(final int imageAngle, final int deviceAngle, final int cameraAngle, final boolean frontCamera, final boolean requireMirroring) {
        final int faceAngle = getFaceOrientationByCamera(imageAngle, deviceAngle, cameraAngle, frontCamera, requireMirroring);
        return ORIENTATION_CREATOR.getOrientation(imageAngle, faceAngle, requireMirroring);
    }

    /**
     * Get face orientation angle value based on image and device orientation values.
     *
     * Image and device orientation angles should be from list of [0, 90, 180, 270].
     * Image orientation angles match to the next head directions:
     * 0 - head-left direction;
     * 90 - head-down direction;
     * 180 - head-right direction;
     * 270 - head-top direction.
     * Device orientation angles match to the device main button direction next way:
     * 0 - button-down direction;
     * 90 - button-right direction;
     * 180 - button-top direction;
     * 270 - button-left direction.
     *
     * @param imageAngle image orientation angle value.
     * @param deviceAngle device orientation angle value.
     * @param requireMirroring mirroring required flag value.
     *
     * @return face orientation angle value, should be one from the list of [0, 90, 180, 270].
     */
    public static int getFaceOrientation(final int imageAngle, final int deviceAngle, final boolean requireMirroring) {
        int faceOrientation = 0;
        if (imageAngle == 90 || imageAngle == 270) {
            // portrait detected
            if (deviceAngle == 90 || deviceAngle == 270) {
                // device orientation landscape
                faceOrientation = (requireMirroring ? 1 : -1) * (imageAngle == 270 ? 1 : -1) * (deviceAngle == 90 ? 90 : -90);
            } else if ((deviceAngle == 180 && imageAngle == 270) || (deviceAngle == 0 && imageAngle == 90)) {
                faceOrientation = 180;
            }
        } else if (imageAngle == 0 || imageAngle == 180) {
            // landscape, check if portrait detected
            if (deviceAngle == 0 || deviceAngle == 180) {
                // device orientation portrait
                faceOrientation = (requireMirroring ? -1 : 1) * (imageAngle == 0 ? 1 : -1) * (deviceAngle == 0 ? 90 : -90);
            } else if ((deviceAngle == 270 && imageAngle == 0) || (deviceAngle == 90 && imageAngle == 180)) {
                faceOrientation = 180;
            }
        }
        return faceOrientation;
    }

    /**
     * Get {@link FullImageData.Orientation} based on image and device orientation values.
     *
     * Image and device orientation angles should be from list of [0, 90, 180, 270].
     * Image orientation angles match to the next head directions:
     * 0 - head-left direction;
     * 90 - head-down direction;
     * 180 - head-right direction;
     * 270 - head-top direction.
     * Device orientation angles match to the device main button direction next way:
     * 0 - button-down direction;
     * 90 - button-right direction;
     * 180 - button-top direction;
     * 270 - button-left direction.
     *
     * @param imageAngle image orientation angle value.
     * @param deviceAngle device orientation angle value.
     * @param requireMirroring mirroring required flag value.
     *
     * @return {@link FullImageData.Orientation}
     */
    public static Orientation getOrientation(final int imageAngle, final int deviceAngle, final boolean requireMirroring) {
        final int faceAngle = getFaceOrientation(imageAngle, deviceAngle, requireMirroring);
        return ORIENTATION_CREATOR.getOrientation(imageAngle, faceAngle, requireMirroring);
    }

    @FunctionalInterface
    private interface OrientationCreator {
        FullImageData.Orientation getOrientation(final int imageAngle, final int faceAngle, final boolean requiredMirroring);
    }
}
