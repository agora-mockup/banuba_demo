package com.banuba.sdk.encoding.sync;

import androidx.annotation.Keep;

import java.util.concurrent.atomic.AtomicInteger;

@Keep
public class EncoderSync implements IEncoderSync {
    private volatile boolean mVideoProcessing;
    private volatile boolean mAudioProcessing;

    private final Object mWait = new Object();

    private final AtomicInteger mEncoderReady = new AtomicInteger(0);


    @Override
    public void setAudioEncoded() {
        mAudioProcessing = false;
        synchronized (mWait) {
            mWait.notify();
        }
    }

    @Override
    public void setVideoEncoded() {
        mVideoProcessing = false;
        synchronized (mWait) {
            mWait.notify();
        }
    }

    @Override
    public void setEncodingStarted() {
        if (mEncoderReady.get() < 2) {
            return;
        }
        mVideoProcessing = true;
        mAudioProcessing = true;
    }

    @Override
    public void waitForEncodingReady() {
        if (mEncoderReady.get() < 2) {
            return;
        }

        if (mVideoProcessing || mAudioProcessing) {
            synchronized (mWait) {
                while (mVideoProcessing || mAudioProcessing) {
                    try {
                        mWait.wait(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void setEncoderReady() {
        mEncoderReady.incrementAndGet();
    }
}
