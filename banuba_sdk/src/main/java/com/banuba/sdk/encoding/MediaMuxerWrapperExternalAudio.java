package com.banuba.sdk.encoding;

import android.media.MediaCodec;
import android.media.MediaFormat;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.banuba.sdk.encoding.sync.IEncoderSync;
import com.banuba.sdk.internal.encoding.MediaMuxerWrapper;
import com.banuba.sdk.internal.encoding.RecordingListenerHandler;
import com.banuba.sdk.internal.renderer.RenderHandler;

import java.io.IOException;
import java.nio.ByteBuffer;

@Keep
public class MediaMuxerWrapperExternalAudio extends MediaMuxerWrapper {
    public MediaMuxerWrapperExternalAudio(@Nullable RenderHandler handler, @Nullable RecordingListenerHandler recordingListenerHandler, @NonNull String fileName, @Nullable IEncoderSync encoderSync, long timeBase, float speed, int w, int h, @Nullable MediaFormat formatVideo, @Nullable MediaFormat formatAudio) throws IOException {
        super(handler, recordingListenerHandler, fileName, MediaMuxerWrapper.RECORD_EXTERNAL_AUDIO_CODEC, encoderSync, timeBase, speed, w, h, formatVideo, formatAudio);
    }

    @Keep
    public synchronized boolean writeAudioSampleData(@NonNull final ByteBuffer byteBuf, @NonNull final MediaCodec.BufferInfo bufferInfo) {
        if (mStartedCount > 0) {
            mMediaMuxer.writeSampleData(mExternalAudioTrackIndex, byteBuf, bufferInfo);
            mAudioPresentationTimeUsLast = bufferInfo.presentationTimeUs;
            return true;
        }
        return false;
    }
}
