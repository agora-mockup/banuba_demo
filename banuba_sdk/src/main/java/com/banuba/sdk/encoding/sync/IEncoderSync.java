package com.banuba.sdk.encoding.sync;

import androidx.annotation.Keep;

@Keep
public interface IEncoderSync {
    void setAudioEncoded();

    void setVideoEncoded();

    void setEncodingStarted();

    void waitForEncodingReady();

    void setEncoderReady();
}
