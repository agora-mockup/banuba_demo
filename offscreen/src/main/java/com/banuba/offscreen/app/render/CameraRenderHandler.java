package com.banuba.offscreen.app.render;

import android.os.Message;

import androidx.annotation.NonNull;

import com.banuba.offscreen.app.utils.BaseWeakHandler;

public class CameraRenderHandler extends BaseWeakHandler<ICameraRenderer> {
    private static final int MSG_SHUTDOWN = 0;
    private static final int MSG_SURFACE_CREATED = 1;
    private static final int MSG_SURFACE_CHANGED = 2;
    private static final int MSG_SURFACE_DESTROYED = 3;
    private static final int MSG_DRAW_FRAME = 4;

    public CameraRenderHandler(@NonNull ICameraRenderer cameraRenderer) {
        super(cameraRenderer);
    }

    public void sendSurfaceCreated() {
        sendMessage(obtainMessage(MSG_SURFACE_CREATED));
    }

    public void sendSurfaceChanged(int width, int height) {
        sendMessage(obtainMessage(MSG_SURFACE_CHANGED, width, height));
    }

    public void sendSurfaceDestroyed() {
        sendMessage(obtainMessage(MSG_SURFACE_DESTROYED));
    }

    public void sendDrawFrame() {
        sendMessage(obtainMessage(MSG_DRAW_FRAME));
    }

    public void sendShutdown() {
        removeCallbacksAndMessages(null); // SHUTDOWN is top priority message, all others should be dropped
        sendMessage(obtainMessage(MSG_SHUTDOWN));
    }

    @Override // runs on RenderThread
    public void handleMessage(@NonNull Message msg) {
        final ICameraRenderer thread = getThread();
        if (thread != null && msg.getCallback() == null) {
            switch (msg.what) {
                case MSG_SHUTDOWN:
                    thread.shutdown();
                    break;
                case MSG_SURFACE_CREATED:
                    thread.handleSurfaceCreated();
                    break;
                case MSG_SURFACE_CHANGED:
                    thread.handleSurfaceChanged(msg.arg1, msg.arg2);
                    break;
                case MSG_SURFACE_DESTROYED:
                    thread.handleSurfaceDestroyed();
                    break;
                case MSG_DRAW_FRAME:
                    thread.handleDrawFrame();
                    break;
                default:
                    throw new RuntimeException("Unknown message " + msg.what);
            }
        }
    }
}
