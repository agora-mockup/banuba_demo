package com.banuba.offscreen.app.adapters;

import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.banuba.offscreen.R;

import java.io.File;
import java.util.List;

public class IconViewAdapter extends RecyclerView.Adapter<IconViewAdapter.EffectInfoViewHolder> {
    private static final int STATE_AVAILABLE = 1;
    private static final int STATE_SELECTED = 2;
    private static final int STATE_UNAVAILABLE = 3;

    @NonNull
    private final List<IconViewAdapterInfo> mInfoList;
    private OnItemClickListener mCallback = null;

    public IconViewAdapter(@NonNull List<IconViewAdapterInfo> infoList) {
        mInfoList = infoList;
    }

    public void setOnItemClickListener(OnItemClickListener callback) {
        mCallback = callback;
    }

    @NonNull
    @Override
    public EffectInfoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_bottom_list, null);
        return new EffectInfoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EffectInfoViewHolder holder, int position) {
        final IconViewAdapterInfo info = mInfoList.get(position);
        holder.setViewInfo(info, STATE_AVAILABLE);
        holder.itemView.setOnClickListener($ -> mCallback.onItemClick(info.getKey()));
    }

    @Override
    public int getItemCount() {
        return mInfoList.size();
    }

    /*package*/ class EffectInfoViewHolder extends RecyclerView.ViewHolder {
        private final FrameLayout mContainer;
        private final ImageView mBackgroundView;
        private final ImageView mImageView;
        private final TextView mTextView;

        void setViewInfo(IconViewAdapterInfo info, int state) {
            mContainer.setVisibility(View.VISIBLE);
            final int resourceId = info.getIconResoureId();
            if (resourceId != 0) {
                mImageView.setImageResource(resourceId);
                mImageView.setVisibility(View.VISIBLE);
                mTextView.setVisibility(View.GONE);
            } else {
                final File icon = new File(info.getIconPath());
                if (icon.exists()) {
                    mImageView.setImageBitmap(BitmapFactory.decodeFile(icon.getPath()));
                    mImageView.setVisibility(View.VISIBLE);
                    mTextView.setVisibility(View.GONE);
                } else {
                    mTextView.setText(info.getKey());
                    mImageView.setVisibility(View.GONE);
                    mTextView.setVisibility(View.VISIBLE);
                }
            }

            updateBackground(info, STATE_AVAILABLE);
        }

        void updateBackground(IconViewAdapterInfo info, int buttonState) {
            int resourceId = 0;
            switch (buttonState) {
                case STATE_AVAILABLE:
                    resourceId = info.getBackgroundAvailableResoureId();
                    break;
                case STATE_SELECTED:
                    resourceId = info.getBackgroundSelectedResoureId();
                    break;
                case STATE_UNAVAILABLE:
                    resourceId = info.getBackgroundUnavailableResoureId();
                    break;
            }
            if (resourceId != 0) {
                mBackgroundView.setImageResource(resourceId);
            }
        }

        EffectInfoViewHolder(View itemView) {
            super(itemView);
            mContainer = itemView.findViewById(R.id.bottom_list_container);
            mBackgroundView = itemView.findViewById(R.id.bottom_list_image_background_view);
            mImageView = itemView.findViewById(R.id.bottom_list_image_view);
            mTextView = itemView.findViewById(R.id.bottom_list_text_view);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(String key);
    }
}
