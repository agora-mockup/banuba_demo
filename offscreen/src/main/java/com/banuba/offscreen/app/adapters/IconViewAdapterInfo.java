package com.banuba.offscreen.app.adapters;

public class IconViewAdapterInfo {
    private final String mKey;
    private final String mIconPath;
    private final int mIconResourceId;
    private final int mBgAvailableResourceId;
    private final int mBgSelectedResourceId;
    private final int mBgUnavailableResourceId;

    public IconViewAdapterInfo(String key, String iconPath, int bgAvailRcId, int bgSelRcId, int bgUnavailRcId) {
        mKey = key;
        mIconPath = iconPath;
        mIconResourceId = 0;
        mBgAvailableResourceId = bgAvailRcId;
        mBgSelectedResourceId = bgSelRcId;
        mBgUnavailableResourceId = bgUnavailRcId;
    }

    public IconViewAdapterInfo(String key, int iconRcId, int bgAvailRcId, int bgSelRcId, int bgUnavailRcId) {
        mKey = key;
        mIconPath = "";
        mIconResourceId = iconRcId;
        mBgAvailableResourceId = bgAvailRcId;
        mBgSelectedResourceId = bgSelRcId;
        mBgUnavailableResourceId = bgUnavailRcId;
    }

    public String getKey() {
        return mKey;
    }

    public String getIconPath() {
        return mIconPath;
    }

    public int getIconResoureId() {
        return mIconResourceId;
    }

    public int getBackgroundAvailableResoureId() {
        return mBgAvailableResourceId;
    }

    public int getBackgroundSelectedResoureId() {
        return mBgSelectedResourceId;
    }

    public int getBackgroundUnavailableResoureId() {
        return mBgUnavailableResourceId;
    }
}
