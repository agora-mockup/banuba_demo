package com.banuba.offscreen.app.fragments;

import static com.banuba.offscreen.app.DemoApplication.BNB_KEY;
import static com.banuba.offscreen.app.DemoApplication.SIZE;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.ImageFormat;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.banuba.offscreen.R;
import com.banuba.offscreen.app.BuffersQueue;
import com.banuba.offscreen.app.DemoApplication;
import com.banuba.offscreen.app.camera.CameraWrapper;
import com.banuba.offscreen.app.render.CameraRenderHandler;
import com.banuba.offscreen.app.render.CameraRenderSurfaceTexture;
import com.banuba.offscreen.app.render.CameraRenderYUVBuffers;
import com.banuba.sdk.effect_player.EffectPlayer;
import com.banuba.sdk.effect_player.EffectPlayerConfiguration;
import com.banuba.sdk.effect_player.NnMode;
import com.banuba.sdk.manager.BanubaSdkManager;
import com.banuba.sdk.offscreen.OffscreenEffectPlayer;
import com.banuba.sdk.offscreen.OffscreenSimpleConfig;
import com.banuba.sdk.recognizer.FaceSearchMode;

import java.util.Objects;


public class CameraFragment extends Fragment {
    private static final String ARG_USE_SURFACE = "surface";
    private static final String ARG_OUTPUT_IMAGE_FORMAT = "output_image_format";

    private static final String EFFECT_NAME = "test_BG";

    private final OffscreenEffectPlayer mOffscreenEffectPlayer;
    private final CameraWrapper mCamera;
    private final BuffersQueue mBuffersQueue;

    private CameraRenderHandler mRenderHandler;

    private boolean mEffectLoaded;

    private final EffectPlayer mEffectPlayer;

    public static Fragment newInstance(boolean useSurface, @Nullable Integer outputImageFormat) {
        final Fragment fragment = new CameraFragment();
        final Bundle args = new Bundle();
        args.putBoolean(ARG_USE_SURFACE, useSurface);
        if (outputImageFormat != null) {
            args.putInt(ARG_OUTPUT_IMAGE_FORMAT, outputImageFormat);
        }
        fragment.setArguments(args);
        return fragment;
    }

    public CameraFragment() {
        final Context context = DemoApplication.getAppContext();
        mBuffersQueue = new BuffersQueue();

        // NOTE: for initializing the EffectPlayer inside the Offscreen Effect Player uncomment next code block
        /*final OffscreenEffectPlayerConfig config =
            OffscreenEffectPlayerConfig.newBuilder(SIZE, mBuffersQueue).build();
        mOffscreenEffectPlayer = new OffscreenEffectPlayer(mContext, config, BNB_KEY);
        */

        final EffectPlayerConfiguration config =
            new EffectPlayerConfiguration(SIZE.getWidth(), SIZE.getHeight(), NnMode.ENABLE, FaceSearchMode.GOOD, false, false);

        BanubaSdkManager.initialize(context, BNB_KEY);
        mEffectPlayer = Objects.requireNonNull(EffectPlayer.create(config));
        mOffscreenEffectPlayer = new OffscreenEffectPlayer(
            context, mEffectPlayer, SIZE, OffscreenSimpleConfig.newBuilder(mBuffersQueue).build());

        mCamera = new CameraWrapper(context, mOffscreenEffectPlayer, SIZE, getLifecycle());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_camera, container, false);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        final SurfaceView mSurfaceView = view.findViewById(R.id.camera_fragment_surface_view);
        final SurfaceHolder holder = mSurfaceView.getHolder();
        final boolean useSurface = getArguments() != null && getArguments().getBoolean(ARG_USE_SURFACE, false);
        final Integer outputImageFormat = getArguments() != null && getArguments().containsKey(ARG_OUTPUT_IMAGE_FORMAT) ? getArguments().getInt(ARG_OUTPUT_IMAGE_FORMAT, ImageFormat.YUV_420_888) : null;

        TextView mFrxFpsLabel = view.findViewById(R.id.activity_main_text_frx_fps);
        TextView mCameraFpsLabel = view.findViewById(R.id.activity_main_text_camera_fps);
        TextView mDrawFpsLabel = view.findViewById(R.id.activity_main_text_draw_fps);
        final FPSValuesProvider fpsValuesProvider = new FPSValuesProvider(new Handler(), mFrxFpsLabel, mCameraFpsLabel, mDrawFpsLabel);
        if (mEffectPlayer != null) {
            mEffectPlayer.addFrameDurationListener(fpsValuesProvider);
        }

        // clang-format off
        mRenderHandler = useSurface ?
                new CameraRenderSurfaceTexture(view.getContext(), SIZE, mOffscreenEffectPlayer, holder, fpsValuesProvider).startAndGetHandler() :
                new CameraRenderYUVBuffers(view.getContext(), SIZE, mOffscreenEffectPlayer, holder, mBuffersQueue, fpsValuesProvider).startAndGetHandler();
        // clang-format on

        final ImageButton btnSwitchCamera = view.findViewById(R.id.fragment_camera_btn_switch_camera);
        btnSwitchCamera.setOnClickListener(v -> mCamera.sendSwitchCamera());

        final Button btnEffect = view.findViewById(R.id.fragment_camera_button_show_effect);
        btnEffect.setOnClickListener(v -> {
            if (mEffectLoaded) {
                mOffscreenEffectPlayer.unloadEffect();
                btnEffect.setText(R.string.show_effect);
            } else {
                mOffscreenEffectPlayer.loadEffect(EFFECT_NAME);
                // NOTE: Using the code below you can combine any mask (properly converted) with
                // virtual background. The code below demonstrates blur VBG.
                // mOffscreenEffectPlayer.evalJs("Background = require('bnb_js/background')", null);
                // mOffscreenEffectPlayer.callJsMethod("Background.blur", "0.8");
                // or you can call it using evalJs
                // mOffscreenEffectPlayer.evalJs("Background.blur(0.8)", null);

                btnEffect.setText(R.string.hide_effect);
            }
            mEffectLoaded = !mEffectLoaded;
        });

        mCamera.applyImageFormat(outputImageFormat);
    }

    private boolean isAndroidOrientationFixed() {
        final Activity activity = getActivity();
        if (activity != null) {
            if (activity.getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR || activity.getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR) {
                return false;
            } else if (activity.getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED) {
                return Settings.System.getInt(activity.getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0) == 0;
            }
        }

        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mRenderHandler.sendShutdown();
        mOffscreenEffectPlayer.release();
    }
}
