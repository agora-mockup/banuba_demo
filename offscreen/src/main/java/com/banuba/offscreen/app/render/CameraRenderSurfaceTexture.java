package com.banuba.offscreen.app.render;

import static android.opengl.EGL14.EGL_NO_CONTEXT;
import static android.opengl.EGL14.eglGetCurrentContext;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.util.Size;
import android.view.SurfaceHolder;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;

import com.banuba.offscreen.app.fragments.DrawDurationListener;
import com.banuba.sdk.internal.gl.GLFullRectTexture;
import com.banuba.sdk.offscreen.OffscreenEffectPlayer;
import com.banuba.sdk.internal.gl.GlUtils;

public class CameraRenderSurfaceTexture extends BaseCameraRenderThread {
    private GLFullRectTexture mGLDrawTextureRGBExt;
    private int mExtTextureId;
    private SurfaceTexture mSurfaceTexture;
    private int mSurfaceRotationIndex;
    @NonNull
    private final DrawDurationListener mDrawListener;

    private long mDrawTimeNano = 0;

    public CameraRenderSurfaceTexture(
        @NonNull Context context, @NonNull Size size, @NonNull OffscreenEffectPlayer offscreenEffectPlayer, @NonNull SurfaceHolder holder, @NonNull DrawDurationListener durationListener) {
        super(context, size, offscreenEffectPlayer, holder);
        mDrawListener = durationListener;
    }

    @WorkerThread
    protected void preRunInit() {
        super.preRunInit();

        mGLDrawTextureRGBExt = new GLFullRectTexture(true);

        mExtTextureId = GlUtils.createExternalTextureObject();
        mSurfaceTexture = new SurfaceTexture(mExtTextureId);

        mSurfaceTexture.setOnFrameAvailableListener(surfaceTexture -> {
            if (eglGetCurrentContext() != EGL_NO_CONTEXT) {
                try {
                    // Call updateTexImage without OpenGL context will result in an error
                    // OpenGLRenderer: [SurfaceTexture] checkAndUpdateEglState: invalid current EGLDisplay
                    surfaceTexture.updateTexImage();
                    mSurfaceRotationIndex = (int) surfaceTexture.getTimestamp() & 0x3;
                    mHandler.sendDrawFrame();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, mHandler);

        mOffscreenEffectPlayer.setSurfaceTexture(mSurfaceTexture);

        mDrawTimeNano = System.nanoTime();
    }


    @Override
    public void handleDrawFrame() {
        if (mWindowSurface != null) {
            GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
            if (mSurfaceRotationIndex == mDefaultDisplay.getRotation()) {
                mGLDrawTextureRGBExt.draw(mExtTextureId);

                final long currentTime = System.nanoTime();
                mDrawListener.onDrawDurationChanged((currentTime - mDrawTimeNano) / 1_000_000_000F);
                mDrawTimeNano = currentTime;
            }
            mWindowSurface.swapBuffers();
        }
    }

    @Override
    protected void postRunClear() {
        super.postRunClear();
        mOffscreenEffectPlayer.setSurfaceTexture(null);
        mSurfaceTexture.release();
    }
}
