package com.banuba.offscreen.app.fragments;

import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.banuba.sdk.effect_player.FrameDurationListener;

import java.util.Locale;

public class FPSValuesProvider implements FrameDurationListener, DrawDurationListener {
    private static final String TAG = "FPSValuesProvider";
    private static final int UPDATE_DURATION_LABEL_FREQUENCY = 15;
    private static final boolean PERFORMANCE_LOGGER_ENABLED = true;
    private static final long PERFORMANCE_LOGGER_INTERVAL_SEC = 1L;

    private int mRecognizerFrameDurationCounter = 0;
    private int mCameraFrameDurationCounter = 0;
    private int mRendererFrameDurationCounter = 0;

    private float mRecognizerFrameDurationLastAverage = 0;
    private float mCameraFrameDurationLastAverage = 0;
    private float mRendererFrameDurationLastAverage = 0;
    private int mPerformanceLoggerTicks = 0;

    private final StringBuilder mRecognizerFrameDurationStringBuilder = new StringBuilder();
    private final StringBuilder mCameraFrameDurationStringBuilder = new StringBuilder();
    private final StringBuilder mRendererFrameDurationStringBuilder = new StringBuilder();

    @NonNull
    private Handler mHandler;
    private final TextView mFrxFpsLabel;
    private final TextView mCameraFpsLabel;
    private final TextView mDrawFpsLabel;

    private final Runnable mPerformanceLogger = () -> {
        final String formattedMessage = String.format(
            Locale.ENGLISH,
            "%d sec, %.1f, %.1f, %.1f",
            mPerformanceLoggerTicks * PERFORMANCE_LOGGER_INTERVAL_SEC,
            1F / mRecognizerFrameDurationLastAverage,
            1F / mCameraFrameDurationLastAverage,
            1F / mRendererFrameDurationLastAverage);
        Log.i(TAG, formattedMessage);
        ++mPerformanceLoggerTicks;
        mHandler.postDelayed(this.mPerformanceLogger, PERFORMANCE_LOGGER_INTERVAL_SEC * 1000);
    };

    FPSValuesProvider(@NonNull Handler handler, TextView frxLabel, TextView cameraLabel, TextView drawLabel) {
        mHandler = handler;
        mFrxFpsLabel = frxLabel;
        mCameraFpsLabel = cameraLabel;
        mDrawFpsLabel = drawLabel;
        if (PERFORMANCE_LOGGER_ENABLED) {
            handler.post(mPerformanceLogger);
        }
    }

    @Override
    public void onRecognizerFrameDurationChanged(float instant, float averaged) {
        if (mRecognizerFrameDurationCounter < UPDATE_DURATION_LABEL_FREQUENCY) {
            ++mRecognizerFrameDurationCounter;
        } else {
            mRecognizerFrameDurationCounter = 0;
            updateFpsLabel(mFrxFpsLabel, "FRX", averaged, mRecognizerFrameDurationStringBuilder);
        }
        mRecognizerFrameDurationLastAverage = averaged;
    }

    @Override
    public void onCameraFrameDurationChanged(float instant, float averaged) {
        if (mCameraFrameDurationCounter < UPDATE_DURATION_LABEL_FREQUENCY) {
            ++mCameraFrameDurationCounter;
        } else {
            mCameraFrameDurationCounter = 0;
            updateFpsLabel(mCameraFpsLabel, "Camera", averaged, mCameraFrameDurationStringBuilder);
        }
        mCameraFrameDurationLastAverage = averaged;
    }

    @Override
    public void onRenderFrameDurationChanged(float instant, float averaged) {
        // not needed because render frame duration comes from #onDrawDurationChanged callback
    }

    @Override
    public void onDrawDurationChanged(float averaged) {
        if (mRendererFrameDurationCounter < UPDATE_DURATION_LABEL_FREQUENCY) {
            ++mRendererFrameDurationCounter;
        } else {
            mRendererFrameDurationCounter = 0;
            updateFpsLabel(mDrawFpsLabel, "Draw", averaged, mRendererFrameDurationStringBuilder);
        }
        mRendererFrameDurationLastAverage = averaged;
    }

    private void updateFpsLabel(
        TextView view, String caption, float averageDuration, StringBuilder labelStringBuilder) {
        mHandler.post(() -> {
            float fps = 1.f / averageDuration;

            labelStringBuilder.setLength(0);
            labelStringBuilder.append(caption).append(": ").append(fps);

            view.setText(labelStringBuilder);
        });
    }
}
