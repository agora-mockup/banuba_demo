package com.banuba.offscreen.app.fragments;

import static com.banuba.offscreen.app.DemoApplication.BNB_KEY;
import static com.banuba.offscreen.app.DemoApplication.SIZE;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.ImageFormat;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.banuba.offscreen.R;
import com.banuba.offscreen.app.BuffersQueue;
import com.banuba.offscreen.app.DemoApplication;
import com.banuba.offscreen.app.adapters.IconViewAdapter;
import com.banuba.offscreen.app.adapters.IconViewAdapterInfo;
import com.banuba.offscreen.app.camera.CameraWrapper;
import com.banuba.offscreen.app.render.CameraRenderHandler;
import com.banuba.offscreen.app.render.CameraRenderYUVBuffers;
import com.banuba.sdk.effect_player.EffectPlayer;
import com.banuba.sdk.effect_player.FrameDataListener;
import com.banuba.sdk.effect_player.EffectPlayerConfiguration;
import com.banuba.sdk.effect_player.FrameDurationListener;
import com.banuba.sdk.effect_player.NnMode;
import com.banuba.sdk.manager.BanubaSdkManager;
import com.banuba.sdk.manager.EffectInfo;
import com.banuba.sdk.offscreen.OffscreenEffectPlayer;
import com.banuba.sdk.offscreen.OffscreenSimpleConfig;
import com.banuba.sdk.recognizer.FaceSearchMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CameraFragmentTestBackground extends Fragment {
    private final BuffersQueue mBuffersQueue = new BuffersQueue();
    private final Context mContext = DemoApplication.getAppContext();
    private final String mPathToEffects = mContext.getFilesDir().toString() + "/banuba/bnb-resources/effects/";
    private final BanubaSdkManager mSdkManager;
    private final EffectPlayer mEffectPlayer;
    private final OffscreenEffectPlayer mOEP;
    private final CameraWrapper mCamera;

    private TextView mEffectText;
    private TextView mJsCallText;

    private CameraRenderHandler mRenderHandler;

    private RecyclerView mEffectsView;
    private RecyclerView mActionsView;

    private MakeupJsHelper mMakeupState = new MakeupJsHelper();

    public CameraFragmentTestBackground() {
        BanubaSdkManager.initialize(mContext, BNB_KEY);

        final EffectPlayerConfiguration config = new EffectPlayerConfiguration(SIZE.getWidth(), SIZE.getHeight(), NnMode.ENABLE, FaceSearchMode.GOOD, false, false);

        mSdkManager = new BanubaSdkManager(mContext);
        mEffectPlayer = Objects.requireNonNull(EffectPlayer.create(config));
        mOEP = new OffscreenEffectPlayer(mContext, mEffectPlayer, SIZE, OffscreenSimpleConfig.newBuilder(mBuffersQueue).build());
        mCamera = new CameraWrapper(mContext, mOEP, SIZE, getLifecycle());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_camera_test_background, container, false);
    }

    private void doAction(String key) {
        final int separatorIndex = key.indexOf(':');
        final String act = key.substring(0, separatorIndex);
        final String arg = key.substring(separatorIndex + 1);
        String script = "";

        if (act.equals("load_effect")) {
            mOEP.loadEffect(arg);
            mEffectText.setText("Effect: " + arg);
            script = "Background = require('bnb_js/background')";
        } else if (act.equals("unload_effect")) {
            mOEP.unloadEffect();
            mEffectText.setText("Effect:");
            mJsCallText.setText("JS:");
            return;
        } else if (act.equals("bg_clear")) {
            script = "Background.clear()";
        } else if (act.equals("bg_set")) {
            if (arg.equals("blur")) {
                script = "Background.blur(1)";
            } else if (arg.equals("open")) {
                final String filePath = getBackgroundFromFile();
                if (filePath == "") {
                    return;
                }
                script = "Background.texture('" + filePath + "')";
            } else {
                script = "Background.texture('" + arg + "')";
            }
        } else if (act.equals("bg_rotate")) {
            script = "Background.rotation(" + arg + ")";
        } else if (act.equals("bg_mode")) {
            script = "Background.contentMode('" + arg + "')";
        } else if (act.equals("bg_scale")) {
            script = "Background.scale('" + arg + "')";
        } else if (act.equals("makeup")) {
            if (arg.equals("clear")) {
                script = mMakeupState.clear();
            } else if (arg.equals("eyes_color")) {
                script = mMakeupState.eyesColor();
            } else if (arg.equals("hair_single_color")) {
                script = mMakeupState.hairColor();
            } else if (arg.equals("hair_multi_color")) {
                script = mMakeupState.hairMultiColor();
            } else if (arg.equals("eyelashes")) {
                script = mMakeupState.eyelashesColor();
            } else if (arg.equals("face_morph")) {
                script = mMakeupState.faceMorph();
            } else if (arg.equals("skin_softening")) {
                script = mMakeupState.skinSoftening();
            } else if (arg.equals("skin_color")) {
                script = mMakeupState.skinColor();
            } else if (arg.equals("brows_color")) {
                script = mMakeupState.browsColor();
            } else if (arg.equals("lips_color")) {
                script = mMakeupState.lipsColor();
            } else if (arg.equals("teeth_whitening")) {
                script = mMakeupState.teethWhitening();
            }
        }

        mOEP.evalJs(script, null);
        mJsCallText.setText("JS: " + script);
    }

    private String getBackgroundFromFile() {
        return "";
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mEffectText = view.findViewById(R.id.camera_fragment_test_bg_text_effect_name);
        mEffectText.setText("Effect:");

        mJsCallText = view.findViewById(R.id.camera_fragment_test_bg_text_js_call);
        mJsCallText.setText("JS:");

        TextView cameraFpsText = view.findViewById(R.id.camera_fragment_test_bg_text_camera_fps);
        TextView drawFpsText = view.findViewById(R.id.camera_fragment_test_bg_text_draw_fps);
        TextView frxFpsText = view.findViewById(R.id.camera_fragment_test_bg_text_frx_fps);

        Handler handler = new Handler();
        mEffectPlayer.addFrameDurationListener(new FrameDurationListener() {
            @Override
            public void onCameraFrameDurationChanged(float instant, float averaged) {
                handler.post(() -> { cameraFpsText.setText("Camera FPS: " + 1.0f / averaged); });
            }

            @Override
            public void onRenderFrameDurationChanged(float instant, float averaged) {
                handler.post(() -> { drawFpsText.setText("Draw FPS: " + 1.0f / averaged); });
            }

            @Override
            public void onRecognizerFrameDurationChanged(float instant, float averaged) {
                handler.post(() -> { frxFpsText.setText("Frx FPS: " + 1.0f / averaged); });
            }
        });

        final SurfaceView surfaceView = view.findViewById(R.id.camera_fragment_test_background_surface_view);
        final SurfaceHolder holder = surfaceView.getHolder();

        List<IconViewAdapterInfo> viewInfoList = new ArrayList<>();
        viewInfoList.add(new IconViewAdapterInfo("unload_effect:", R.mipmap.background_cancel, 0, 0, 0));
        List<EffectInfo> effectInfoList = mSdkManager.loadEffects();
        for (EffectInfo effectInfo : effectInfoList) {
            viewInfoList.add(new IconViewAdapterInfo("load_effect:" + effectInfo.getName(), new File(mPathToEffects, effectInfo.previewImage()).toString(), 0, 0, 0));
        }

        IconViewAdapter.OnItemClickListener onItemClickListener = new IconViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String key) {
                doAction(key);
            }
        };

        List<IconViewAdapterInfo> actionsList = new ArrayList<>();
        final int bg_avail = R.mipmap.background_avail;
        final int bg_sel = R.mipmap.background_sel;
        final int bg_unavail = R.mipmap.background_unavail;
        final int mk_avail = R.mipmap.makeup_avail;
        // background
        actionsList.add(new IconViewAdapterInfo("bg_clear:", R.mipmap.background_cancel, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_set:../../vbg_samples/png_image.png", R.mipmap.background_png, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_set:../../vbg_samples/jpg_image.jpg", R.mipmap.background_jpg, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_set:../../vbg_samples/jpeg_image.jpeg", R.mipmap.background_jpeg, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_set:../../vbg_samples/gif_image.gif", R.mipmap.background_gif, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_set:../../vbg_samples/mp4_video.mp4", R.mipmap.background_mp4, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_set:../../vbg_samples/mkv_video.mkv", R.mipmap.background_mkv, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_set:../../vbg_samples/wmv_video.wmv", R.mipmap.background_wmv, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_set:../../vbg_samples/webm_video.webm", R.mipmap.background_webm, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_set:../../vbg_samples/avi_video.avi", R.mipmap.background_avi, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_set:../../vbg_samples/mov_video.mov", R.mipmap.background_mov, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_set:../../vbg_samples/mp4_video_with_upper_case_ext.MP4", R.mipmap.background_mp4, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_set:../../vbg_samples/mp4_video_with_upper_case_ext.mp4", R.mipmap.background_mp4, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_set:blur", R.mipmap.background_blur, bg_avail, bg_sel, bg_unavail));
        // actionsList.add(new IconViewAdapterInfo("bg_set:open", R.mipmap.background_open, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_rotate:0", R.mipmap.background_0deg_right, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_rotate:90", R.mipmap.background_90deg_right, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_rotate:180", R.mipmap.background_180deg_right, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_rotate:270", R.mipmap.background_270deg_right, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_mode:fill", R.mipmap.background_fill, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_mode:fit", R.mipmap.background_fit, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_mode:scale_to_fill", R.mipmap.background_scale_to_fill, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_scale:0.5", R.mipmap.background_scale_x05, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_scale:1", R.mipmap.background_scale_x1, bg_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("bg_scale:2", R.mipmap.background_scale_x2, bg_avail, bg_sel, bg_unavail));
        // makeup
        actionsList.add(new IconViewAdapterInfo("makeup:clear", R.mipmap.background_cancel, mk_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("makeup:eyes_color", R.mipmap.makeup_eyes, mk_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("makeup:hair_single_color", R.mipmap.makeup_hair_color_1, mk_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("makeup:hair_multi_color", R.mipmap.makeup_hair_color_2, mk_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("makeup:eyelashes", R.mipmap.makeup_eyelashes, mk_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("makeup:face_morph", 0, mk_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("makeup:skin_softening", 0, mk_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("makeup:skin_color", 0, mk_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("makeup:brows_color", 0, mk_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("makeup:lips_color", 0, mk_avail, bg_sel, bg_unavail));
        actionsList.add(new IconViewAdapterInfo("makeup:teeth_whitening", 0, mk_avail, bg_sel, bg_unavail));

        mEffectsView = view.findViewById(R.id.bottom_effects_list);
        mEffectsView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        final IconViewAdapter effectsAdapter = new IconViewAdapter(viewInfoList);
        mEffectsView.setAdapter(effectsAdapter);
        effectsAdapter.setOnItemClickListener(onItemClickListener);

        mActionsView = view.findViewById(R.id.bottom_actions_list);
        mActionsView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        final IconViewAdapter actionsAdapter = new IconViewAdapter(actionsList);
        mActionsView.setAdapter(actionsAdapter);
        actionsAdapter.setOnItemClickListener(onItemClickListener);

        mRenderHandler = new CameraRenderYUVBuffers(view.getContext(), SIZE, mOEP, holder, mBuffersQueue, null).startAndGetHandler();

        final ImageButton btnSwitchCamera = view.findViewById(R.id.btn_switch_camera);
        btnSwitchCamera.setOnClickListener(v -> mCamera.sendSwitchCamera());
        mCamera.applyImageFormat(ImageFormat.YUV_420_888);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mRenderHandler.sendShutdown();
        mOEP.release();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public static CameraFragmentTestBackground newInstance() {
        return new CameraFragmentTestBackground();
    }

    public static class MakeupJsHelper {
        private boolean mEyesColor = false;
        private boolean mEyelashesColor = false;
        private boolean mHairColor = false;
        private boolean mHairMultiColor = false;
        private boolean mFaceMorph = false;
        private boolean mSkinSoftening = false;
        private boolean mSkinColor = false;
        private boolean mBrowsColor = false;
        private boolean mLipsColor = false;
        private boolean mTeethWhitening = false;

        public String clear() {
            mEyesColor = false;
            mEyelashesColor = false;
            mHairColor = false;
            mHairMultiColor = false;
            mFaceMorph = false;
            mSkinSoftening = false;
            mSkinColor = false;
            mBrowsColor = false;
            mLipsColor = false;
            mTeethWhitening = false;

            return "Eyes.clear(); Hair.clear(); FaceMorph.clear(); Skin.softening(0); Skin.clear(); Brows.clear(); Lips.clear(); Makeup.clear(); Teeth.clear();";
        }

        public String eyesColor() {
            mEyesColor = !mEyesColor;
            if (mEyesColor) {
                return "Eyes.color('0 0.5 0.8 0.84')";
            } else {
                return "Eyes.clear()";
            }
        }

        public String eyelashesColor() {
            mEyelashesColor = !mEyelashesColor;
            if (mEyelashesColor) {
                return "Makeup.lashes('0.6 0 0.6')";
            } else {
                return "Makeup.clear()";
            }
        }

        public String hairColor() {
            mHairColor = !mHairColor;
            if (mHairColor) {
                return "Hair.color('0.59 0.14 0.44 0.9')";
            } else {
                return "Hair.clear()";
            }
        }

        public String hairMultiColor() {
            mHairMultiColor = !mHairMultiColor;
            if (mHairMultiColor) {
                return "Hair.color('0.19 0.06 0.55', '0.09 0.55 0.38')";
            } else {
                return "Hair.clear()";
            }
        }

        public String faceMorph() {
            mFaceMorph = !mFaceMorph;
            if (mFaceMorph) {
                return "FaceMorph.eyes(0.6); FaceMorph.face(0.5); FaceMorph.nose(1); FaceMorph.lips(1);";
            } else {
                return "FaceMorph.clear()";
            }
        }

        public String skinSoftening() {
            mSkinSoftening = !mSkinSoftening;
            if (mSkinSoftening) {
                return "Skin.softening(1)";
            } else {
                return "Skin.softening(0)";
            }
        }

        public String skinColor() {
            mSkinColor = !mSkinColor;
            if (mSkinColor) {
                return "Skin.color('0.1 0.6 0.8 0.6')";
            } else {
                return "Skin.clear()";
            }
        }

        public String browsColor() {
            mBrowsColor = !mBrowsColor;
            if (mBrowsColor) {
                return "Brows.color('0.172 0.125 0.105 0.732')";
            } else {
                return "Brows.clear()";
            }
        }

        public String lipsColor() {
            mLipsColor = !mLipsColor;
            if (mLipsColor) {
                return "Lips.shiny('1 0 0.49 1')";
            } else {
                return "Lips.clear()";
            }
        }

        public String teethWhitening() {
            mTeethWhitening = !mTeethWhitening;
            if (mTeethWhitening) {
                return "Teeth.whitening(1)";
            } else {
                return "Teeth.clear()";
            }
        }
    }
}
