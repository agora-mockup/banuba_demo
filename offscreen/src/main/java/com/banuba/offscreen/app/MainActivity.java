package com.banuba.offscreen.app;

import android.content.Intent;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.banuba.offscreen.R;
import com.banuba.offscreen.app.fragments.CameraFragmentTestBackground;

public class MainActivity extends AppCompatActivity {
    public static final String KEY_OUTPUT_IMAGE_FORMAT = "OUTPUT_IMAGE_FORMAT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.activity_main_btn_test_bg).setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this, CameraActivityTestBackground.class));
        });

        // Buffers YUV -> Unspecified

        findViewById(R.id.activity_main_btn_camera_yuv_to_unspecified_portrait).setOnClickListener(v -> {
            runActivity(CameraActivityBuffersPortrait.class, null);
        });

        findViewById(R.id.activity_main_btn_camera_yuv_to_unspecified_full_sensor).setOnClickListener(v -> {
            runActivity(CameraActivityBuffersFullSensor.class, null);
        });

        findViewById(R.id.activity_main_btn_camera_yuv_to_unspecified_default).setOnClickListener(v -> {
            runActivity(CameraActivityBuffersDefault.class, null);
        });

        // Buffers YUV -> YUV

        findViewById(R.id.activity_main_btn_camera_yuv_to_yuv_portrait).setOnClickListener(v -> {
            runActivity(CameraActivityBuffersPortrait.class, ImageFormat.YUV_420_888);
        });

        findViewById(R.id.activity_main_btn_camera_yuv_to_yuv_full_sensor).setOnClickListener(v -> {
            runActivity(CameraActivityBuffersFullSensor.class, ImageFormat.YUV_420_888);
        });

        findViewById(R.id.activity_main_btn_camera_yuv_to_yuv_default).setOnClickListener(v -> {
            runActivity(CameraActivityBuffersDefault.class, ImageFormat.YUV_420_888);
        });

        // Buffer YUV -> RGBA

        findViewById(R.id.activity_main_btn_camera_yuv_to_rgba_portrait).setOnClickListener(v -> {
            runActivity(CameraActivityBuffersPortrait.class, PixelFormat.RGBA_8888);
        });

        findViewById(R.id.activity_main_btn_camera_yuv_to_rgba_full_sensor).setOnClickListener(v -> {
            runActivity(CameraActivityBuffersFullSensor.class, PixelFormat.RGBA_8888);
        });

        findViewById(R.id.activity_main_btn_camera_yuv_to_rgba_default).setOnClickListener(v -> {
            runActivity(CameraActivityBuffersDefault.class, PixelFormat.RGBA_8888);
        });

        // Surface

        findViewById(R.id.activity_main_btn_camera_yuv_to_surface_portrait).setOnClickListener(v -> {
            runActivity(CameraActivitySurfaceDefault.class, null);
        });

        findViewById(R.id.activity_main_btn_camera_yuv_to_surface_full_sensor).setOnClickListener(v -> {
            runActivity(CameraActivitySurfaceDefault.class, null);
        });

        findViewById(R.id.activity_main_btn_camera_yuv_to_surface_default).setOnClickListener(v -> {
            runActivity(CameraActivitySurfaceDefault.class, null);
        });
    }

    private void runActivity(@NonNull Class<?> cls, @Nullable Integer imageFormat) {
        final Intent intent = new Intent(MainActivity.this, cls);
        if (imageFormat != null) {
            intent.putExtra(KEY_OUTPUT_IMAGE_FORMAT, imageFormat);
        }
        startActivity(intent);
    }
}