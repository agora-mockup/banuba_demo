package com.banuba.offscreen.app.utils;

import android.os.Handler;

import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;

public class BaseWeakHandler<WT> extends Handler {
    private final WeakReference<WT> mWeakThread;

    public BaseWeakHandler(WT wt) {
        mWeakThread = new WeakReference<>(wt);
    }

    @Nullable
    public WT getThread() {
        return mWeakThread.get();
    }
}
