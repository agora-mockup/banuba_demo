package com.banuba.offscreen.app.render;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;
import android.util.Size;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.WindowManager;

import androidx.annotation.CallSuper;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import androidx.lifecycle.LifecycleObserver;

import com.banuba.offscreen.app.gl.GlViewPortSize;
import com.banuba.sdk.internal.BaseWorkThread;
import com.banuba.sdk.internal.gl.EglCore;
import com.banuba.sdk.internal.gl.GlUtils;
import com.banuba.sdk.internal.gl.OffscreenSurface;
import com.banuba.sdk.internal.gl.WindowSurface;
import com.banuba.sdk.offscreen.OffscreenEffectPlayer;

import java.util.Objects;

public abstract class BaseCameraRenderThread extends BaseWorkThread<CameraRenderHandler> implements LifecycleObserver, ICameraRenderer {
    protected final float[] mIdentity = new float[16];

    protected final SurfaceHolder mSurfaceHolder;
    protected final OffscreenEffectPlayer mOffscreenEffectPlayer;
    protected final Size mSize;

    protected CameraRenderHandler mHandler;
    protected WindowSurface mWindowSurface;
    protected EglCore mEglCore;

    protected GlViewPortSize mViewPort169;

    protected final Display mDefaultDisplay;

    private OffscreenSurface mOffscreenSurface;

    public BaseCameraRenderThread(@NonNull Context context, @NonNull Size size, @NonNull OffscreenEffectPlayer offscreenEffectPlayer, @NonNull SurfaceHolder holder) {
        super("CameraRenderThread");
        mSize = size;
        mSurfaceHolder = holder;
        mOffscreenEffectPlayer = offscreenEffectPlayer;

        mDefaultDisplay = Objects.requireNonNull((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Matrix.setIdentityM(mIdentity, 0);

        mSurfaceHolder.addCallback(new SurfaceHolder.Callback() {
            // Always Called on UI Thread

            @MainThread
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                Log.d("BANUBA", "[" + Thread.currentThread().getId() + "] sendSurfaceCreated !!! ");
                mHandler.sendSurfaceCreated();
            }

            @MainThread
            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                Log.d("BANUBA", "[" + Thread.currentThread().getId() + "] sendSurfaceChanged !!! ");
                mHandler.sendSurfaceChanged(width, height);
                mHandler.sendDrawFrame();
            }

            @MainThread
            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                Log.d("BANUBA", "[" + Thread.currentThread().getId() + "] sendSurfaceDestroyed !!! ");
                mHandler.sendSurfaceDestroyed();
            }
        });
    }

    @NonNull
    @Override
    protected CameraRenderHandler constructHandler() {
        mHandler = new CameraRenderHandler(this);
        return mHandler;
    }

    @CallSuper
    @WorkerThread
    protected void preRunInit() {
        Log.d("BANUBA", "[" + Thread.currentThread().getId() + "] BaseCameraRenderThread.preRunInit() ");

        mEglCore = new EglCore(null, 0x2);

        mOffscreenSurface = new OffscreenSurface(mEglCore, 16, 16);
        mOffscreenSurface.makeCurrent();
    }

    @CallSuper
    @WorkerThread
    protected void postRunClear() {
        Log.d("BANUBA", "[" + Thread.currentThread().getId() + "] BaseCameraRenderThread.postRunClear() ");
        mEglCore.makeNothingCurrent();
        if (mOffscreenSurface != null) {
            mOffscreenSurface.release();
        }
        mEglCore.release();
    }

    @WorkerThread
    public final void handleSurfaceCreated() {
        Log.d("BANUBA", "[" + Thread.currentThread().getId() + "] BaseCameraRenderThread.handleSurfaceCreated() ");
        final Surface surface = mSurfaceHolder.getSurface();

        mWindowSurface = new WindowSurface(mEglCore, surface, false);
        mWindowSurface.makeCurrent();

        GLES20.glDisable(GLES20.GL_CULL_FACE);
        GLES20.glDisable(GLES20.GL_BLEND);
        GLES20.glDisable(GLES20.GL_DEPTH_TEST);

        GlUtils.checkGlErrorNoException("prepareGl");

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    }


    @WorkerThread
    public final void handleSurfaceChanged(int width, int height) {
        Log.d("BANUBA", "[" + Thread.currentThread().getId() + "] BaseCameraRenderThread.handleSurfaceChanged(" + width + "x" + height + ")");
        mViewPort169 = GlViewPortSize.makeViewPortWithAspect(width, height, mSize);
        mViewPort169.apply();
    }

    public void handleSurfaceDestroyed() {
        Log.d("BANUBA", "[" + Thread.currentThread().getId() + "] BaseCameraRenderThread.handleSurfaceDestroyed()");
        if (mOffscreenSurface != null) {
            mOffscreenSurface.makeCurrent();
        }
        final WindowSurface windowSurface = mWindowSurface;
        if (windowSurface != null) {
            windowSurface.release();
        }
        mWindowSurface = null;
    }

    public abstract void handleDrawFrame();
}
