package com.banuba.offscreen.app.fragments;

import androidx.annotation.Keep;

@Keep
@FunctionalInterface
public interface DrawDurationListener {
    /**
     * On draw time duration callback.
     *
     * @param averaged average draw time in seconds (float).
     */
    void onDrawDurationChanged(float averaged);
}
