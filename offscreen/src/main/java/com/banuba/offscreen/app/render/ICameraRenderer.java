package com.banuba.offscreen.app.render;

public interface ICameraRenderer {
    void handleSurfaceCreated();

    void handleSurfaceChanged(int width, int height);

    void handleSurfaceDestroyed();

    void handleDrawFrame();

    void shutdown();
}
