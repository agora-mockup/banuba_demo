package com.banuba.offscreen.app;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.banuba.offscreen.app.fragments.CameraFragment;

public class CameraActivityBuffersPortrait extends BaseCameraActivity {
    @NonNull
    @Override
    protected Fragment getCameraFragment(@Nullable Integer outputImageFormat) {
        return CameraFragment.newInstance(false, outputImageFormat);
    }
}
