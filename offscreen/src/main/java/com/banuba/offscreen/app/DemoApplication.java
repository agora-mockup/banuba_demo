package com.banuba.offscreen.app;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.util.Size;

import androidx.annotation.NonNull;

public class DemoApplication extends Application {
    public static final Size SIZE = new Size(1280, 720);
    public static final String BNB_KEY = BanubaClientToken.KEY;

    @SuppressLint("StaticFieldLeak")
    private static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
    }

    @NonNull
    public static Context getAppContext() {
        return sContext;
    }
}
