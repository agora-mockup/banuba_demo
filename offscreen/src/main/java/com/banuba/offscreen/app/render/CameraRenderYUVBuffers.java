package com.banuba.offscreen.app.render;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.opengl.GLES20;
import android.opengl.GLES30;
import android.util.Size;
import android.view.SurfaceHolder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;

import com.banuba.offscreen.app.BuffersQueue;
import com.banuba.offscreen.app.fragments.DrawDurationListener;
import com.banuba.offscreen.app.gl.GLDrawTextureYUVPlanar;
import com.banuba.sdk.internal.gl.GLFullRectTexture;
import com.banuba.sdk.internal.gl.WindowSurface;
import com.banuba.sdk.offscreen.ImageProcessResult;
import com.banuba.sdk.offscreen.OffscreenEffectPlayer;

import java.nio.ByteBuffer;

public class CameraRenderYUVBuffers extends BaseCameraRenderThread {
    private final int[] mTextures = new int[3];
    private final BuffersQueue mBuffersQueue;
    private GLDrawTextureYUVPlanar mGLDrawTextureYUVPlanar;
    private GLFullRectTexture mGLFullRectTexture;

    @NonNull
    private final DrawDurationListener mDrawListener;

    private long mDrawTimeNano = 0;

    public CameraRenderYUVBuffers(
        @NonNull Context context, @NonNull Size size, @NonNull OffscreenEffectPlayer offscreenEffectPlayer, @NonNull SurfaceHolder holder, @Nullable BuffersQueue buffersQueue, @NonNull DrawDurationListener durationListener) {
        super(context, size, offscreenEffectPlayer, holder);
        mBuffersQueue = buffersQueue;
        mDrawListener = durationListener;
    }

    private ImageProcessResult mImageProcessResult;

    @NonNull
    @Override
    protected CameraRenderHandler constructHandler() {
        mHandler = new CameraRenderHandler(this);
        mOffscreenEffectPlayer.setImageProcessListener(result -> {
            if (mImageProcessResult != null && mBuffersQueue != null) {
                mBuffersQueue.retainBuffer(mImageProcessResult.getBuffer());
            }
            mImageProcessResult = result;
            mHandler.sendDrawFrame();
        }, mHandler);
        return mHandler;
    }

    @WorkerThread
    protected void preRunInit() {
        super.preRunInit();

        makeTextures(mTextures);
        mGLDrawTextureYUVPlanar = new GLDrawTextureYUVPlanar(false);
        mGLFullRectTexture = new GLFullRectTexture(false);

        mDrawTimeNano = System.nanoTime();
    }

    @WorkerThread
    protected void postRunClear() {
        super.postRunClear();
        mOffscreenEffectPlayer.setImageProcessListener(null, null);
        mGLDrawTextureYUVPlanar.release();
        GLES20.glDeleteTextures(mTextures.length, mTextures, 0);
    }

    @Override
    public void handleDrawFrame() {
        final ImageProcessResult imageProcessResult = mImageProcessResult;
        final WindowSurface windowSurface = mWindowSurface;

        if (windowSurface != null && imageProcessResult != null) {
            GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
            final int displaySurfaceRotation = mDefaultDisplay.getRotation();
            if (imageProcessResult.getOrientation().getRotationIndex() == displaySurfaceRotation) {
                final float[] matrix = imageProcessResult.getOrientation().getDefaultDrawMatrix(displaySurfaceRotation);
                final int format = imageProcessResult.getFormat();
                if (format == ImageFormat.YUV_420_888) {
                    loadProcessResultYUV2Textures(imageProcessResult);
                    mGLDrawTextureYUVPlanar.draw(false, mTextures, mIdentity, matrix);
                } else if (format == PixelFormat.RGBA_8888) {
                    loadProcessResultRGBA2Textures(imageProcessResult);
                    mGLFullRectTexture.draw(mTextures[0], matrix);
                }

                final long currentTime = System.nanoTime();
                if (mDrawListener != null) {
                    mDrawListener.onDrawDurationChanged((currentTime - mDrawTimeNano) / 1_000_000_000F);
                }
                mDrawTimeNano = currentTime;
            }
            windowSurface.swapBuffers();
        }
    }


    private void loadProcessResultYUV2Textures(@NonNull ImageProcessResult result) {
        final ByteBuffer buffer = result.getBuffer();

        for (int i = 0; i < result.getPlaneCount(); i++) {
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextures[i]);
            GLES30.glPixelStorei(GLES30.GL_UNPACK_ROW_LENGTH, result.getBytesPerRowOfPlane(i));
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE, result.getWidthOfPlane(i), result.getHeightOfPlane(i), 0, GLES20.GL_LUMINANCE, GLES20.GL_UNSIGNED_BYTE, buffer.position(result.getOffsetOfPlane(i)));
        }

        GLES30.glPixelStorei(GLES30.GL_UNPACK_ROW_LENGTH, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
    }

    private void loadProcessResultRGBA2Textures(@NonNull ImageProcessResult result) {
        final ByteBuffer buffer = result.getBuffer();

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextures[0]);
        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, result.getWidthOfPlane(0), result.getHeightOfPlane(0), 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, buffer.position(result.getOffsetOfPlane(0)));
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
    }


    public static void makeTextures(int[] textures) {
        GLES20.glGenTextures(textures.length, textures, 0);

        for (int texture : textures) {
            if (texture == 0) {
                throw new RuntimeException("Error loading texture.");
            }
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        }

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
    }
}
