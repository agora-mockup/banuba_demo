package com.banuba.offscreen.app.camera;

import androidx.annotation.Nullable;

public interface ICamera2Simple {
    void openCameraAndStartPreview();

    void stopPreviewAndCloseCamera();

    void switchFacing();

    void applyOrientationAngles(int sensorAngle, int surfaceRotation);

    void applyOutputImageFormat(@Nullable Integer outputImageFormat);
}
