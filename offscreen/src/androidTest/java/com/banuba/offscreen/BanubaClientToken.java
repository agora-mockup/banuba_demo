package com.banuba.offscreen;

/**
 * Client token for Banuba SDK. Consider obfuscation in release app.
 */
/*package*/ final class BanubaClientToken {
    public static final String KEY = "Put your client token here";

    private BanubaClientToken() {
    }
}