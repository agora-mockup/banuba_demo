package com.banuba.offscreen;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.banuba.offscreen.utils.TestThread;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;

@RunWith(AndroidJUnit4.class)
public class OEPFrameOrientationTest {
    public static final String BNB_KEY = BanubaClientToken.KEY;

    @Test
    public void testRotation0() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        new TestThread(latch, "w1280_h720_sy1280_su1280_sv1280_vfrw1280_vfrh720_vfr0.bin", BNB_KEY).start();
        latch.await();
    }

    @Test
    public void testRotation90() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        new TestThread(latch, "w1280_h720_sy1280_su1280_sv1280_vfrw720_vfrh1280_vfr90.bin", BNB_KEY).start();
        latch.await();
    }
    @Test
    public void testRotation180() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        new TestThread(latch, "w1280_h720_sy1280_su1280_sv1280_vfrw1280_vfrh720_vfr180.bin", BNB_KEY).start();
        latch.await();
    }

    @Test
    public void testRotation270() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        new TestThread(latch, "w1280_h720_sy1280_su1280_sv1280_vfrw720_vfrh1280_vfr270.bin", BNB_KEY).start();
        latch.await();
    }
}