package com.banuba.offscreen.utils;

import android.content.Context;
import android.graphics.ImageFormat;

import androidx.annotation.NonNull;
import androidx.test.platform.app.InstrumentationRegistry;

import com.banuba.sdk.offscreen.ImageDebugUtils;

import org.junit.Assert;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class TestData {
    final int buffer420Width;
    final int buffer420Height;
    final int buffer420StrideY;
    final int buffer420StrideU;
    final int buffer420StrideV;
    final int videoFrameRotatedWidth;
    final int videoFrameRotatedHeight;
    final int videoFrameRotation;

    final ByteBuffer bufferY;
    final ByteBuffer bufferU;
    final ByteBuffer bufferV;

    TestData(@NonNull String filename) {
        final Context contextTarget = InstrumentationRegistry.getInstrumentation().getTargetContext();
        final Context contextTest = InstrumentationRegistry.getInstrumentation().getContext();

        final String filenameNoExtension = filename.replaceAll("\\.\\w+", "");
        final String[] splits = filenameNoExtension.split("_");

        for (int i = 0; i < splits.length; i++) {
            splits[i] = splits[i].replaceAll("[^0-9]", "");
        }

        buffer420Width = Integer.parseInt(splits[0]);
        buffer420Height = Integer.parseInt(splits[1]);
        buffer420StrideY = Integer.parseInt(splits[2]);
        buffer420StrideU = Integer.parseInt(splits[3]);
        buffer420StrideV = Integer.parseInt(splits[4]);

        videoFrameRotatedWidth = Integer.parseInt(splits[5]);
        videoFrameRotatedHeight = Integer.parseInt(splits[6]);
        videoFrameRotation = Integer.parseInt(splits[7]);

        ByteBuffer tempBufferY = null;
        ByteBuffer tempBufferU = null;
        ByteBuffer tempBufferV = null;

        try {
            InputStream is = contextTest.getAssets().open(filename);
            Assert.assertNotNull(is);
            ReadableByteChannel channel = Channels.newChannel(is);

            tempBufferY = ByteBuffer.allocateDirect(buffer420StrideY * buffer420Height);
            tempBufferU = ByteBuffer.allocateDirect(buffer420StrideU * buffer420Height / 2);
            tempBufferV = ByteBuffer.allocateDirect(buffer420StrideV * buffer420Height / 2);

            channel.read(tempBufferY);
            channel.read(tempBufferU);
            channel.read(tempBufferV);

            saveFile(contextTarget, buffer420Width, buffer420Height, buffer420StrideY, tempBufferY, "rot" + videoFrameRotation + "_ch0Y.jpg");
            //            saveFile(contextTarget, buffer420Width / 2, buffer420Height / 2, buffer420StrideU, tempBufferU, "rot" + videoFrameRotation + "_ch1U.jpg");
            //            saveFile(contextTarget, buffer420Width / 2, buffer420Height / 2, buffer420StrideV, tempBufferV, "rot" + videoFrameRotation + "_ch2V.jpg");

            channel.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        Objects.requireNonNull(tempBufferY);
        Objects.requireNonNull(tempBufferU);
        Objects.requireNonNull(tempBufferV);

        bufferY = tempBufferY;
        bufferU = tempBufferU;
        bufferV = tempBufferV;
    }


    private static void saveFile(@NonNull Context context, int width, int height, int stride, @NonNull ByteBuffer buffer, @NonNull String name) {
        final Map<String, String> map = new LinkedHashMap<>();
        map.put("Width", Integer.toString(width));
        map.put("Height", Integer.toString(height));
        ImageDebugUtils.saveImageDetailed(context, ImageFormat.YUV_420_888, buffer, width, height, stride, name, map);
    }
}
