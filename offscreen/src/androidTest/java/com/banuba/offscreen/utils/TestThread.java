package com.banuba.offscreen.utils;

import android.content.Context;
import android.graphics.ImageFormat;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Size;

import androidx.annotation.NonNull;
import androidx.test.platform.app.InstrumentationRegistry;

import com.banuba.sdk.effect_player.CameraOrientation;
import com.banuba.sdk.offscreen.ImageDebugUtils;
import com.banuba.sdk.offscreen.OffscreenEffectPlayer;
import com.banuba.sdk.offscreen.OffscreenEffectPlayerConfig;
import com.banuba.sdk.types.FullImageData;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class TestThread extends Thread {
    private static final String EFFECT_NAME = "test_BG";
    private final CountDownLatch latch;
    private final String key;
    private final String filename;

    public TestThread(@NonNull CountDownLatch latch, @NonNull String filename, @NonNull String key) {
        this.latch = latch;
        this.filename = filename;
        this.key = key;
    }


    @Override
    public void run() {
        final TestData data = new TestData(filename);

        final Context contextTarget = InstrumentationRegistry.getInstrumentation().getTargetContext();

        OffscreenEffectPlayerConfig config = OffscreenEffectPlayerConfig.newBuilder(new Size(1280, 720), null).build();
        OffscreenEffectPlayer player = new OffscreenEffectPlayer(contextTarget, config, key);

        Looper.prepare();
        Handler handler = new Handler();

        final int faceOrientation = 0;
        final CameraOrientation cameraOrientation = CameraOrientation.values()[data.videoFrameRotation / 90];
        final FullImageData.Orientation orientation = new FullImageData.Orientation(cameraOrientation, false, faceOrientation);
        final FullImageData fullImageData = new FullImageData(new Size(data.buffer420Width, data.buffer420Height), data.bufferY, data.bufferU, data.bufferV, data.buffer420StrideY, data.buffer420StrideU, data.buffer420StrideV, 1, 1, 1, orientation);

        player.setImageProcessListener(result -> {}, handler);

        player.processFullImageDataNoSkip(fullImageData, () -> Log.e("ANDREY9", " onRelease called"), 1);

        player.setImageProcessListener(result -> {
            String filename = "rot" + data.videoFrameRotation + "_result.jpg";
            final Map<String, String> map = new LinkedHashMap<>();
            map.put("Rotation", Integer.toString(data.videoFrameRotation));
            ImageDebugUtils.saveImageDetailed(contextTarget, ImageFormat.YUV_420_888, result.getBuffer(), result.getWidth(), result.getHeight(), result.getWidth(), filename, map);
        }, handler);
        player.processFullImageDataNoSkip(fullImageData, () -> Log.e("ANDREY9", " onRelease called"), 2);

        player.loadEffect(EFFECT_NAME);

        player.processFullImageDataNoSkip(fullImageData, () -> Log.e("ANDREY9", " onRelease called"), 3);

        player.setImageProcessListener(result -> {}, handler);


        for (int i = 10; i < 50; i++) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            player.processFullImageDataNoSkip(fullImageData, null, i);
        }


        player.setImageProcessListener(result -> {
            final String filename = "rot" + data.videoFrameRotation + "_result_effect.jpg";
            final Map<String, String> map = new LinkedHashMap<>();
            map.put("Rotation", Integer.toString(data.videoFrameRotation));
            map.put("Effect", "YES");
            ImageDebugUtils.saveImageDetailed(contextTarget, ImageFormat.YUV_420_888, result.getBuffer(), result.getWidth(), result.getHeight(), result.getWidth(), filename, map);
        }, handler);
        player.processFullImageDataNoSkip(fullImageData, null, 50);

        player.setImageProcessListener(result -> {
            final String filename = "rot" + data.videoFrameRotation + "_result_effect_jsBGrotate.jpg";
            ImageDebugUtils.saveImageDetailed(contextTarget, ImageFormat.YUV_420_888, result.getBuffer(), result.getWidth(), result.getHeight(), result.getWidth(), filename, null);
        }, handler);
        player.processFullImageDataNoSkip(fullImageData, null, 60);


        player.setImageProcessListener(result -> {
            Looper looper = Looper.myLooper();
            if (looper != null) {
                looper.quit();
            }
        }, handler);
        player.processFullImageDataNoSkip(fullImageData, null, 99);


        Looper.loop();

        latch.countDown();
    }
}
