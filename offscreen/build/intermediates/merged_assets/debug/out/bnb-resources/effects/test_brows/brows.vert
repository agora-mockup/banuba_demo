#include <bnb/glsl.vert>

BNB_LAYOUT_LOCATION(0) BNB_IN vec3 attrib_pos;
BNB_LAYOUT_LOCATION(1) BNB_IN vec2 uv;
BNB_LAYOUT_LOCATION(2) BNB_IN vec2 brows_uv;



BNB_OUT(0) vec2 var_uv;

void main()
{
	var_uv = brows_uv;
    gl_Position = bnb_MVP * vec4( attrib_pos, 1. );
}