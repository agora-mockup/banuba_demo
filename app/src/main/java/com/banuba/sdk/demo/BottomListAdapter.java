package com.banuba.sdk.demo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.List;

public class BottomListAdapter
    extends RecyclerView.Adapter<BottomListAdapter.EffectInfoViewHolder> {
    private boolean mIsNeedToShowAddButton;
    @NonNull
    private List<Effect> mInfos;
    @NonNull
    private OnEffectClickListener mCallback;

    public BottomListAdapter(
        boolean isNeedToShowAddButton,
        @NonNull List<Effect> infos,
        @NonNull OnEffectClickListener callback) {
        mIsNeedToShowAddButton = isNeedToShowAddButton;
        mInfos = infos;
        mCallback = callback;
    }

    public interface OnEffectClickListener {
        void onAddEffectClick();

        void onEffectClick(Effect effect);
    }

    @NonNull
    @Override
    public EffectInfoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_bottom_list, null);
        return new EffectInfoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EffectInfoViewHolder holder, int position) {
        if (mIsNeedToShowAddButton && position == mInfos.size()) {
            holder.mImageView.setImageResource(R.drawable.ic_add);
            holder.itemView.setOnClickListener($ -> mCallback.onAddEffectClick());
        } else {
            Effect effect = mInfos.get(position);

            String previewPath = effect.getAdditionalPath() + effect.getEffectInfo().previewImage();
            File preview = new File(previewPath);
            if (preview.exists()) {
                holder.setBitmap(BitmapFactory.decodeFile(preview.getPath()));
            }

            holder.itemView.setOnClickListener($ -> mCallback.onEffectClick(effect));
        }
    }

    @Override
    public int getItemCount() {
        return mInfos.size() + getAdditionalIndex();
    }

    /*package*/ class EffectInfoViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImageView;

        void setBitmap(Bitmap bitmap) {
            mImageView.setImageBitmap(bitmap);
        }

        EffectInfoViewHolder(View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.imageView);
        }
    }

    private int getAdditionalIndex() {
        return mIsNeedToShowAddButton ? 1 : 0; //+1 because added "Add effect" item
    }
}