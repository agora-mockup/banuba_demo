package com.banuba.sdk.demo.editing;

import com.banuba.sdk.effect_player.CameraOrientation;

public class ImageAttrs {
    private String path;
    private int width;
    private int height;
    private CameraOrientation orientation;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public CameraOrientation getOrientation() {
        return orientation;
    }

    public void setOrientation(CameraOrientation orientation) {
        this.orientation = orientation;
    }
}
