package com.banuba.sdk.demo.editing;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.banuba.sdk.demo.R;

import java.util.List;

public class GalleryActivity
    extends AppCompatActivity implements LoadGalleryDataTask.OnGalleryLoadListener,
                                         GalleryAdapter.OnGalleryItemClickListener {
    private static int ASK_STORAGE_PERMISSION_CODE = 10234;

    private LoadGalleryDataTask loadGalleryDataTask;

    private View progressBar;
    private RecyclerView galleryRecyclerView;
    private GalleryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        progressBar = findViewById(R.id.progressBar);

        findViewById(R.id.closeGalleryBtn).setOnClickListener((v) -> { finish(); });
        findViewById(R.id.updateGalleryBtn).setOnClickListener((v) -> {
            checkStoragePermission();
        });

        setupRecyclerView();
        checkStoragePermission();
    }

    @Override
    protected void onDestroy() {
        if (loadGalleryDataTask != null) {
            loadGalleryDataTask.cancel(true);
        }
        super.onDestroy();
    }

    private void setupRecyclerView() {
        galleryRecyclerView = findViewById(R.id.galleryRecyclerView);
        final RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 3);
        adapter = new GalleryAdapter();
        adapter.setOnItemClickListener(this);
        galleryRecyclerView.setAdapter(adapter);
        galleryRecyclerView.setHasFixedSize(true);
        galleryRecyclerView.setLayoutManager(layoutManager);
    }

    private void loadGalleryData() {
        if (loadGalleryDataTask != null) {
            loadGalleryDataTask.cancel(true);
        }
        loadGalleryDataTask = new LoadGalleryDataTask(getApplicationContext(), this);
        loadGalleryDataTask.execute();
    }

    private void checkStoragePermission() {
        if (isStoragePermissionGranted()) {
            loadGalleryData();
        } else {
            ActivityCompat.requestPermissions(
                this,
                new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},
                ASK_STORAGE_PERMISSION_CODE);
        }
    }

    private boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            return ContextCompat.checkSelfPermission(
                       this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(
        int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
            && requestCode == ASK_STORAGE_PERMISSION_CODE) {
            loadGalleryData();
        }
    }

    @Override
    public void onGalleryItemClick(String path) {
        Fragment editingFragmentCurrent = getSupportFragmentManager().findFragmentByTag("EditingFragmentTag");
        if (editingFragmentCurrent != null) {
            getSupportFragmentManager().beginTransaction().remove(editingFragmentCurrent).commit();
        }
        Fragment editingFragment = EditingFragment.newInstance(path);
        getSupportFragmentManager()
            .beginTransaction()
            .add(R.id.fragmentContainer, editingFragment, "EditingFragmentTag")
            .addToBackStack("EditingFragment")
            .commit();
    }

    @Override
    public void onGalleryLoadStart() {
        progressBar.setVisibility(View.VISIBLE);
        galleryRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void onGalleryLoadFinish(List<String> list) {
        progressBar.setVisibility(View.GONE);
        galleryRecyclerView.setVisibility(View.VISIBLE);
        adapter.updateGalleryItems(list);
    }
}
