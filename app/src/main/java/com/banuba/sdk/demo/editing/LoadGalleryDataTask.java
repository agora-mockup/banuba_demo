package com.banuba.sdk.demo.editing;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;

import java.util.LinkedList;
import java.util.List;

class LoadGalleryDataTask extends AsyncTask<Void, Void, List<String>> {
    interface OnGalleryLoadListener {
        void onGalleryLoadStart();
        void onGalleryLoadFinish(List<String> list);
    }

    private Context context;
    private OnGalleryLoadListener loadListener;


    public LoadGalleryDataTask(Context context, OnGalleryLoadListener loadListener) {
        this.context = context;
        this.loadListener = loadListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        loadListener.onGalleryLoadStart();
    }

    @Override
    protected List<String> doInBackground(Void... params) {
        return getGalleryData(true);
    }

    @Override
    protected void onPostExecute(List<String> result) {
        super.onPostExecute(result);
        loadListener.onGalleryLoadFinish(result);
    }

    private List<String> getGalleryData(boolean isPhoto) {
        List<String> galleryItems = new LinkedList<String>();
        final String[] projection = new String[] {MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME};
        Uri uri = isPhoto ? MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                          : MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        String sortOrder =
            isPhoto ? MediaStore.Images.Media.DATE_MODIFIED : MediaStore.Video.Media.DATE_MODIFIED;
        sortOrder += " DESC";
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, sortOrder);
        if (cursor != null) {
            galleryItems.addAll(retrieveGalleryPaths(cursor));
            cursor.close();
        }
        return galleryItems;
    }

    private List<String> retrieveGalleryPaths(Cursor cursor) {
        List<String> galleryItems = new LinkedList<String>();
        int columnData = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        while (cursor.moveToNext()) {
            String fileName = cursor.getString(columnData);
            galleryItems.add(fileName);
        }
        return galleryItems;
    }
}