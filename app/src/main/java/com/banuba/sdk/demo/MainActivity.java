package com.banuba.sdk.demo;

import android.Manifest;
import android.Manifest.permission;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.util.Size;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.banuba.sdk.camera.CameraFpsMode;
import com.banuba.sdk.camera.Facing;
import com.banuba.sdk.demo.editing.GalleryActivity;
import com.banuba.sdk.effect_player.CameraOrientation;
import com.banuba.sdk.effect_player.EffectActivationCompletionListener;
import com.banuba.sdk.effect_player.EffectInfoListener;
import com.banuba.sdk.effect_player.EffectManager;
import com.banuba.sdk.effect_player.FrameDataListener;
import com.banuba.sdk.effect_player.ProcessImageParams;
import com.banuba.sdk.entity.GravityPositionProviderAdapter;
import com.banuba.sdk.entity.PositionProvider;
import com.banuba.sdk.entity.RecordedVideoInfo;
import com.banuba.sdk.entity.SizeProvider;
import com.banuba.sdk.entity.WatermarkInfo;
import com.banuba.sdk.manager.BanubaSdkManager;
import com.banuba.sdk.manager.BanubaSdkManagerConfiguration;
import com.banuba.sdk.manager.BanubaSdkTouchListener;
import com.banuba.sdk.manager.EffectInfo;
import com.banuba.sdk.manager.IEventCallback;
import com.banuba.sdk.recognizer.UtilityManager;
import com.banuba.sdk.types.Data;
import com.banuba.sdk.types.FullImageData;
import com.developer.filepicker.model.DialogConfigs;
import com.developer.filepicker.model.DialogProperties;
import com.developer.filepicker.view.FilePickerDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ir.mahdi.mzip.zip.ZipArchive;

import static android.os.Build.VERSION;

public class MainActivity extends AppCompatActivity implements IEventCallback {
    private static final String TAG = "MainActivity";

    private static final int REQUEST_WRITE_STORAGE = 112;
    private static final int REQUEST_CODE_PERMISSION = 10072;
    private static final int REQUEST_CODE_SELECT_IMAGE = 10;

    private static final SimpleDateFormat TIME_BASED_FILE_NAME_FORMAT =
        new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss", Locale.ENGLISH);
    private static final String VIDEO_EXT = ".mp4";

    public static long sPhotoRequestTimeMs;

    private TextView mHintText;
    private TextView mEffectText;
    private TextView mEffectRendererText;
    private TextView mEffectFeaturesText;
    private TextView mEffectUsesAudio;
    private TextView mEffectUsesVideo;
    private TextView mEffectUsesTouches;
    private TextView mDeviceHardwareClass;
    private RecyclerView mBottomList;
    private TextView mFrameDurationLoggerButtonNote;
    private TextView mFixedFpsButtonNote;
    private BanubaSdkManager mSdkManager;

    private View mShutterButton;
    private View mRecordBinsButton;

    private boolean mIsPhotoMode = true;
    private boolean mFakePhoto = false;
    private boolean mIsFrontCamera = true;

    private FrameDurationLogger mFrameDurationLogger = null;
    private FrameDataListener mRulerListener = null;

    private Handler mHandler;

    private UnzipEffectHolderDTO unzipEffectHolderDTO;

    private final EffectActivationCompletionListener effectActivationCompletionListener = (url) -> {
        Log.w("bnb_sdk", "onEffectActivationFinished() " + url);
    };

    @SuppressLint("SetTextI18n")
    private final EffectInfoListener effectInfoListener = (info) -> {
        runOnUiThread(() -> {
            mEffectFeaturesText.setText("Features: " + info.getRecognizerFeatures().toString());
            mEffectUsesAudio.setText("Effect uses audio: " + info.getUsesAudio());
            mEffectUsesVideo.setText("Effect uses video: " + info.getUsesVideo());
            mEffectUsesTouches.setText("Effect uses touches: " + info.getUsesTouches());
            mDeviceHardwareClass.setText("Device hardware class: " + UtilityManager.getHardwareClass());
        });
    };

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        // NOTE: for using specific configuration next code block may be uncommented
        /*BanubaSdkManagerConfiguration configuration = BanubaSdkManagerConfiguration.newInstance(Facing.FRONT, true)
            .setResolutionController(size -> new Size(720, 1280))
            .setFpsController((sortedAvailableFpsRanges, proposedRange) -> Range.create(15, 60))
            .setAutoRotationHandler(this::isAutoRotationOff)
            .build();
        mSdkManager = new BanubaSdkManager(this, configuration);
        */

        mSdkManager = new BanubaSdkManager(this, BanubaSdkManagerConfiguration.newInstance().setAutoRotationHandler(this::isAutoRotationOff).build());

        mSdkManager.setCallback(this);

        TextView mFrxFpsLabel = findViewById(R.id.activity_main_text_frx_fps);
        TextView mCameraFpsLabel = findViewById(R.id.activity_main_text_camera_fps);
        TextView mDrawFpsLabel = findViewById(R.id.activity_main_text_draw_fps);
        mBottomList = findViewById(R.id.bottom_list);
        mHintText = findViewById(R.id.activity_main_text_hint);
        mEffectText = findViewById(R.id.activity_main_text_effect_name);
        mShutterButton = findViewById(R.id.shutterButton);
        mEffectFeaturesText = findViewById(R.id.debugInfoFeaturesText);
        mEffectRendererText = findViewById(R.id.debugInfoRenderedText);
        mEffectUsesAudio = findViewById(R.id.debugInfoUsesAudio);
        mEffectUsesVideo = findViewById(R.id.debugInfoUsesVideo);
        mEffectUsesTouches = findViewById(R.id.debugInfoUsesTouches);
        mDeviceHardwareClass = findViewById(R.id.debugInfoHardwareClass);

        findViewById(R.id.debugInfoParentView).setOnLongClickListener(view -> {
            final int effectInfoVisibility;
            if (mEffectFeaturesText.getVisibility() == View.VISIBLE) {
                effectInfoVisibility = View.GONE;
            } else {
                effectInfoVisibility = View.VISIBLE;
            }
            mEffectFeaturesText.setVisibility(effectInfoVisibility);
            mEffectRendererText.setVisibility(effectInfoVisibility);
            mEffectUsesAudio.setVisibility(effectInfoVisibility);
            mEffectUsesVideo.setVisibility(effectInfoVisibility);
            mEffectUsesTouches.setVisibility(effectInfoVisibility);
            mDeviceHardwareClass.setVisibility(effectInfoVisibility);
            return true;
        });

        mFrameDurationLoggerButtonNote = findViewById(R.id.frameDurationLoggerButtonNote);
        findViewById(R.id.frameDurationLoggerButton).setOnClickListener(view -> {
            onDurationLoggerButtonClick();
        });

        mFixedFpsButtonNote = findViewById(R.id.fixedFpsButtonNote);
        findViewById(R.id.fixedFpsButton).setOnClickListener(view -> {
            onFixedFpsButtonClick();
        });

        setFpsButtonNote(CameraFpsMode.DEFAULT == CameraFpsMode.FIXED);

        TextView cameraSwitchButtonNote = findViewById(R.id.cameraSwitchButtonNote);
        findViewById(R.id.cameraSwitchButton).setOnClickListener(view -> {
            findViewById(R.id.hqPhotoButton).setClickable(false);

            if (!mIsFrontCamera) {
                mIsFrontCamera = mSdkManager.setCameraFacing(Facing.FRONT, true);
                cameraSwitchButtonNote.setText(getString(R.string.front_camera));
            } else {
                mIsFrontCamera = !mSdkManager.setCameraFacing(Facing.BACK, false);
                cameraSwitchButtonNote.setText(getString(R.string.back_camera));
            }

            cameraSwitchButtonNote.setText(
                mIsFrontCamera ? getString(R.string.front_camera) : getString(R.string.back_camera));

            view.setClickable(false);
        });

        View redShutterButton = findViewById(R.id.redShutterButton);
        View cameraModeButton = findViewById(R.id.cameraModeButton);
        cameraModeButton.setOnClickListener(view -> {
            mIsPhotoMode = !mIsPhotoMode;

            if (mIsPhotoMode) {
                redShutterButton.setVisibility(View.GONE);

                Toast.makeText(this, getString(R.string.phote_mode), Toast.LENGTH_SHORT).show();
            } else {
                redShutterButton.setVisibility(View.VISIBLE);

                Toast.makeText(this, getString(R.string.video_mode), Toast.LENGTH_SHORT).show();
            }
        });

        setupShutterButton();
        setupCameraActions();

        mHandler = new Handler();

        mSdkManager.getEffectPlayer().addFrameDurationListener(
            new FPSValuesProvider(mHandler, mFrxFpsLabel, mCameraFpsLabel, mDrawFpsLabel));
        mSdkManager.getEffectPlayer().effectManager().addHintListener(new HintListener());
        mSdkManager.getEffectPlayer().addEffectInfoListener(effectInfoListener);
        mSdkManager.getEffectPlayer().addEffectActivationCompletionListener(effectActivationCompletionListener);

        final SurfaceView sv = findViewById(R.id.activity_main_surface_view);
        mSdkManager.attachSurface(sv);
        sv.setOnTouchListener(new BanubaSdkTouchListener(this, mSdkManager.getEffectPlayer()));

        setEffectsInfo();

        requestPermission();

        mIsFrontCamera = mSdkManager.getCameraFacing() == Facing.FRONT;
    }

    public void takeFakePhoto() {
        runOnUiThread(() -> {
            mFakePhoto = true;
            findViewById(R.id.hqPhotoButton).performClick();
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setupShutterButton() {
        mShutterButton.setOnTouchListener(($, motionEvent) -> {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (!mIsPhotoMode) {
                        startVideoRecord();
                    }
                    break;

                case MotionEvent.ACTION_UP:
                    if (mIsPhotoMode) {
                        takePhoto();
                    } else {
                        stopVideoRecord();
                    }
                    break;
            }

            return true;
        });
    }

    private void startVideoRecord() {
        String fileName = getTimeBasedFileName(VIDEO_EXT);
        File file = new File(getExternalCacheDir(), fileName);

        WatermarkInfo info = createWatermarkInfo();
        mSdkManager.setWatermarkInfo(info);

        String fileNamePath = getWatermarkFileName(file.getAbsolutePath());
        boolean isPermissionGranted = isRecordAudioPermissionGranted();

        mSdkManager.startVideoRecording(fileNamePath, isPermissionGranted, null, 1.0f);
    }

    private void stopVideoRecord() {
        mSdkManager.stopVideoRecording();
    }

    public void takePhoto() {
        sPhotoRequestTimeMs = System.currentTimeMillis();
        mSdkManager.setWatermarkInfo(createWatermarkInfo());
        mSdkManager.takePhoto(null);
    }

    private WatermarkInfo createWatermarkInfo() {
        Drawable watermarkDrawable = ContextCompat.getDrawable(this, R.drawable.ic_new_watermark);
        int height = (int) getResources().getDimension(R.dimen.watermark_height);
        int width = (int) getResources().getDimension(R.dimen.watermark_width);
        float aspectRatio = 1f * width / height;
        SizeProvider provider = viewportSize -> {
            float targetWidth = viewportSize.getWidth() * 0.35f;
            float targetHeight = targetWidth / aspectRatio;
            return new Size((int) targetWidth, (int) targetHeight);
        };
        PositionProvider positionProvider =
            new GravityPositionProviderAdapter(provider, Gravity.BOTTOM | Gravity.RIGHT);
        return new WatermarkInfo(
            watermarkDrawable, provider, positionProvider, width, height, true);
    }

    private String getWatermarkFileName(String fileName) {
        int extSymbolIndex = fileName.lastIndexOf(".");
        return fileName.substring(0, extSymbolIndex) + "_watermark"
            + fileName.substring(extSymbolIndex);
    }

    private void setupCameraActions() {
        findViewById(R.id.hqPhotoButton).setOnClickListener((v) -> {
            sPhotoRequestTimeMs = System.currentTimeMillis();
            mSdkManager.effectPlayerPause();
            mSdkManager.processCameraPhoto(new ProcessImageParams(false, null, null));
        });

        findViewById(R.id.galleryButton).setOnClickListener((v) -> {
            startActivity(new Intent(this, GalleryActivity.class));
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSdkManager.effectPlayerPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSdkManager.openCamera();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSdkManager.effectPlayerPlay();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // NOTE: additional effectPlayerPause call is needed to stop the effect sound
        // in Multi-Window mode when another application is expanded on the whole screen
        mSdkManager.effectPlayerPause();

        mSdkManager.closeCamera();
    }

    @Override
    protected void onDestroy() {
        mSdkManager.getEffectPlayer().removeEffectInfoListener(effectInfoListener);
        mSdkManager.getEffectPlayer().removeEffectActivationCompletionListener(effectActivationCompletionListener);
        mSdkManager.recycle();
        mSdkManager = null;
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Process image
        if (requestCode == REQUEST_CODE_SELECT_IMAGE && resultCode == RESULT_OK && data != null
            && data.getData() != null) {
            sPhotoRequestTimeMs = System.currentTimeMillis();
            Uri uri = data.getData();
            try {
                CameraOrientation cameraOrientation = CameraOrientation.DEG_0;

                if (VERSION.SDK_INT >= 24) {
                    // check the orientation
                    ParcelFileDescriptor descriptor =
                        getContentResolver().openFileDescriptor(uri, "r");
                    assert descriptor != null;
                    ExifInterface exif =
                        new ExifInterface(new FileInputStream(descriptor.getFileDescriptor()));
                    int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            cameraOrientation = CameraOrientation.DEG_270;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            cameraOrientation = CameraOrientation.DEG_180;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            cameraOrientation = CameraOrientation.DEG_90;
                            break;
                    }
                }

                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                mSdkManager.processImage(
                    new FullImageData(bitmap, new FullImageData.Orientation(cameraOrientation)),
                    new ProcessImageParams(false, null, null));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("SetTextI18n")
    public void setEffectsInfo() {
        List<EffectInfo> effectInfoList = mSdkManager.loadEffects();
        mBottomList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        List<Effect> effects = new ArrayList<>();
        for (EffectInfo effectInfo : effectInfoList) {
            effects.add(new Effect(mSdkManager.getResourcesBase() + "/effects/", effectInfo));
        }

        BottomListAdapter.OnEffectClickListener callback = new BottomListAdapter.OnEffectClickListener() {
            @Override
            public void onAddEffectClick() {
                DialogProperties properties = new DialogProperties();

                properties.selection_mode = DialogConfigs.SINGLE_MODE;
                properties.selection_type = DialogConfigs.FILE_AND_DIR_SELECT;
                properties.root = Environment.getExternalStorageDirectory();
                properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
                properties.offset = new File(DialogConfigs.DEFAULT_DIR);
                properties.extensions = new String[] {"zip"};

                FilePickerDialog dialog = new FilePickerDialog(MainActivity.this, properties);
                dialog.setTitle("Select an effect folder or .zip file");

                dialog.setDialogSelectionListener(paths -> {
                    String pathToEffect = paths[0];

                    boolean isFile = pathToEffect.lastIndexOf(".") > 0;
                    if (isFile) {
                        String selectedFileExtension = pathToEffect.substring(pathToEffect.lastIndexOf("."));
                        if (selectedFileExtension.equals(".zip")) {
                            String destinationPath = pathToEffect.replace(".zip", "");

                            unzipEffectToFolder(
                                pathToEffect,
                                destinationPath,
                                effects);
                        }
                    } else {
                        addEffectToList(pathToEffect, effects);
                    }
                });

                dialog.show();
            }

            @Override
            public void onEffectClick(Effect effect) {
                String fullPath = effect.getAdditionalPath() + effect.getEffectInfo().getPath();
                com.banuba.sdk.effect_player.EffectInfo info = EffectManager.getEffectInfo(fullPath);
                Log.i(TAG, "Effect info before loading effect");
                Log.i(TAG, "Url: " + info.getUrl());
                Log.i(TAG, "Uses audio: " + info.getUsesAudio());
                Log.i(TAG, "Uses video: " + info.getUsesVideo());
                Log.i(TAG, "Uses touches: " + info.getUsesTouches());

                mSdkManager.loadEffect(fullPath, false);

                String effectName = effect.getEffectInfo().getName();
                mEffectText.setText("Effect: " + effectName);
                mEffectText.setVisibility(View.VISIBLE);

                mRulerListener = RulerValuesProvider.updateRulerListener(effectName, mSdkManager, mRulerListener);
            }
        };

        BottomListAdapter adapter = new BottomListAdapter(true, effects, callback);

        mBottomList.setAdapter(adapter);
    }

    private void addEffectToList(String path, List<Effect> effects) {
        EffectInfo newEffect = new EffectInfo(path);
        effects.add(new Effect("", newEffect));

        if (mBottomList.getAdapter() != null) {
            mBottomList.getAdapter().notifyDataSetChanged();
        }

        Toast.makeText(
                 MainActivity.this,
                 "Mask added at the end of the list",
                 Toast.LENGTH_LONG)
            .show();
    }

    private void unzipEffectToFolder(
        String pathToEffect,
        String destinationPath,
        List<Effect> effects) {
        boolean hasPermission = (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

        if (!hasPermission) {
            unzipEffectHolderDTO = new UnzipEffectHolderDTO(
                pathToEffect,
                destinationPath,
                effects);

            ActivityCompat.requestPermissions(
                this,
                new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_WRITE_STORAGE);
        } else {
            Thread thread = new Thread(() -> {
                ZipArchive.unzip(pathToEffect, destinationPath, "");
                runOnUiThread(() -> addEffectToList(destinationPath, effects));
            });
            thread.start();
        }
    }

    @Override
    public void onCameraOpenError(@NonNull Throwable error) {
        Log.e(TAG, "Failed to open camera. " + error.getMessage());
    }

    @Override
    public void onCameraStatus(boolean opened) {
        findViewById(R.id.cameraSwitchButton).setClickable(opened);
        findViewById(R.id.hqPhotoButton).setClickable(opened);
    }

    @Override
    public void onHQPhotoReady(@NonNull Bitmap photo) {
        if (mFakePhoto) {
            mFakePhoto = false;
            mSdkManager.effectPlayerPlay();
            return;
        }

        Intent intent = new Intent(this, PreviewActivity.class);
        PreviewActivity.bitmap = photo;
        startActivity(intent);
    }

    @Override
    public void onVideoRecordingFinished(@NonNull RecordedVideoInfo videoInfo) {
        Uri uri = FileProvider.getUriForFile(
            this, "com.banuba.sdk.demo", new File(videoInfo.getFilePath()));
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.setDataAndType(uri, "video/mp4");
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION:
                final boolean resultCamera = isCameraPermissionGranted();
                if (resultCamera) {
                    mSdkManager.openCamera();
                } else {
                    Log.e(TAG, "No camera permissions");
                }

            case REQUEST_WRITE_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (unzipEffectHolderDTO != null) {
                        unzipEffectToFolder(
                            unzipEffectHolderDTO.pathToEffect,
                            unzipEffectHolderDTO.destinationPath,
                            unzipEffectHolderDTO.effects);
                    }
                } else {
                    Toast.makeText(this, "Write to external not allowed", Toast.LENGTH_LONG).show();
                }
            }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onVideoRecordingStatusChange(boolean started) {
        Log.i(TAG, "onVideoRecordingStatusChange");
    }

    @Override
    public void onScreenshotReady(@NonNull Bitmap photo) {
        Log.i(TAG, "onScreenshotReady");
        Intent intent = new Intent(this, PreviewActivity.class);
        PreviewActivity.bitmap = photo;
        startActivity(intent);
    }

    private boolean isCameraPermissionGranted() {
        if (VERSION.SDK_INT >= 23) {
            return checkSelfPermission(permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    private boolean isRecordAudioPermissionGranted() {
        if (VERSION.SDK_INT >= 23) {
            return checkSelfPermission(permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    @Override
    public void onImageProcessed(@NonNull Bitmap bitmap) {
        Intent intent = new Intent(this, PreviewActivity.class);
        PreviewActivity.bitmap = bitmap;
        startActivity(intent);
    }

    @Override
    public void onEditedImageReady(@NonNull Bitmap image) {
        Log.i(TAG, "onEditedImageReady");
    }

    @Override
    public void onEditingModeFaceFound(boolean faceFound) {
        Log.i(TAG, "onEditingModeFaceFound: " + faceFound);
    }

    @Override
    public void onFrameRendered(@NonNull Data data, int width, int height) {
        data.close();
    }

    private void requestPermission() {
        String[] permissionsList = null;

        if (!isCameraPermissionGranted() && !isRecordAudioPermissionGranted()) {
            permissionsList = new String[] {permission.CAMERA, permission.RECORD_AUDIO};
        } else if (!isRecordAudioPermissionGranted()) {
            permissionsList = new String[] {permission.RECORD_AUDIO};
        } else if (!isCameraPermissionGranted()) {
            permissionsList = new String[] {permission.CAMERA};
        }
        if (permissionsList != null) {
            ActivityCompat.requestPermissions(
                this, permissionsList, REQUEST_CODE_PERMISSION);
        }
    }

    public void onDurationLoggerButtonClick() {
        if (mFrameDurationLogger == null) {
            mFrameDurationLogger = new FrameDurationLogger();
            mSdkManager.getEffectPlayer().addFrameDurationListener(mFrameDurationLogger);

            mFrameDurationLoggerButtonNote.setText(R.string.frame_logger_on);
            Toast.makeText(this, getString(R.string.frame_logger_enabled), Toast.LENGTH_SHORT).show();
        } else {
            mSdkManager.getEffectPlayer().removeFrameDurationListener(mFrameDurationLogger);
            mFrameDurationLogger.print();
            mFrameDurationLogger = null;

            mFrameDurationLoggerButtonNote.setText(R.string.frame_logger_off);
            Toast.makeText(this, getString(R.string.frame_logger_disabled), Toast.LENGTH_SHORT).show();
        }
    }


    public void onFixedFpsButtonClick() {
        setFpsButtonNote(!mFixedFpsButtonNote.isActivated());
    }

    private boolean isAutoRotationOff() {
        return Settings.System.getInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0) != 1;
    }

    private void setFpsButtonNote(boolean isActivated) {
        mFixedFpsButtonNote.setActivated(isActivated);
        if (mFixedFpsButtonNote.isActivated()) {
            mFixedFpsButtonNote.setText(R.string.fixed_fps_on);
            mSdkManager.setCameraFpsMode(CameraFpsMode.FIXED);
        } else {
            mFixedFpsButtonNote.setText(R.string.fixed_fps_off);
            mSdkManager.setCameraFpsMode(CameraFpsMode.ADAPTIVE);
        }
    }

    //
    // HintListener
    //

    private class HintListener implements com.banuba.sdk.effect_player.HintListener {
        @Override
        public void onHint(@NonNull String hintMessage) {
            mHandler.post(() -> mHintText.setText(hintMessage));
        }
    }

    private String getTimeBasedFileName(String extension) {
        String name = TIME_BASED_FILE_NAME_FORMAT.format(new Date());
        return name + extension;
    }

    private class UnzipEffectHolderDTO {
        private String pathToEffect;
        private String destinationPath;
        private List<Effect> effects;

        private UnzipEffectHolderDTO(String pathToEffect, String destinationPath, List<Effect> effects) {
            this.pathToEffect = pathToEffect;
            this.destinationPath = destinationPath;
            this.effects = effects;
        }
    }
}