package com.banuba.sdk.demo;

import androidx.annotation.NonNull;
import android.util.Log;

import com.banuba.sdk.effect_player.FrameDurationListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

class FrameDurationLogger implements FrameDurationListener {
    private static final String TAG = "FrameDurationLogger";

    static private class PerfData {
        private final float minInit = Float.MAX_VALUE;
        private final float maxInit = 0.0f;

        List<Float> values = new ArrayList<>();

        float minDuration = minInit;
        float maxDuration = maxInit;
        float valuesSum = 0;

        boolean isValid() {
            return minInit != minDuration && maxInit != maxDuration && values.size() > 0;
        }

        float averaged() {
            return valuesSum / values.size();
        }

        void newValue(float duration) {
            values.add(duration);

            if (minDuration > duration) {
                minDuration = duration;
            }

            if (maxDuration < duration) {
                maxDuration = duration;
            }

            valuesSum += duration;
        }
    }

    static class OutputFormatter {
        private final StringBuilder mData = new StringBuilder();
        private static final char column_separator = ';';
        private static final char row_separator = '\n';

        OutputFormatter(@NonNull List<List<Float>> values) {
            int columnsCount = values.size();
            int[] columnSizes = new int[columnsCount];
            int maxColumnSize = 0;

            for (int i = 0; i < columnsCount; ++i) {
                columnSizes[i] = values.get(i).size();
                maxColumnSize = Math.max(maxColumnSize, columnSizes[i]);
            }

            for (int row = 0; row < maxColumnSize; ++row) {
                if (columnsCount > 1) {
                    for (int col = 0; col < columnsCount - 1; ++col) {
                        List<Float> columnValues = values.get(col);
                        int columnSize = columnSizes[col];

                        mData.append(
                            row >= columnSize ? fmtEmptyCell(FmtPosition.Default)
                                              : fmtVal(columnValues.get(row), FmtPosition.Default));
                    }
                }

                List<Float> lastColumnValues = values.get(columnsCount - 1);
                int lastColumnSize = columnSizes[columnsCount - 1];

                mData.append(
                    row >= lastColumnSize ? fmtEmptyCell(FmtPosition.Last)
                                          : fmtVal(lastColumnValues.get(row), FmtPosition.Last));
            }
        }

        @NonNull
        @Override
        public String toString() {
            return mData.toString();
        }

        enum FmtPosition { Default,
                           Last }

        @NonNull
        static String fmtVal(float v, FmtPosition pos) {
            String fmt_str = "%6s" + (pos == FmtPosition.Last ? row_separator : column_separator);
            DecimalFormat decimalFormat = new DecimalFormat("0.000");
            return String.format(fmt_str, decimalFormat.format(v));
        }

        @NonNull
        static String fmtEmptyCell(FmtPosition pos) {
            String fmt_str = "%6s" + (pos == FmtPosition.Last ? row_separator : column_separator);
            return String.format(fmt_str, "");
        }
    }

    class DurationValuesCollection {
        final PerfData recognizer = new PerfData();
        final PerfData camera = new PerfData();
        final PerfData render = new PerfData();

        boolean isValid() {
            return recognizer.isValid() && camera.isValid() && render.isValid();
        }

        private void printLine(float v1, float v2, float v3) {
            Log.i(
                TAG,
                OutputFormatter.fmtVal(v1, OutputFormatter.FmtPosition.Default)
                    + OutputFormatter.fmtVal(v2, OutputFormatter.FmtPosition.Default)
                    + OutputFormatter.fmtVal(v3, OutputFormatter.FmtPosition.Last));
        }

        void printValues() {
            List<List<Float>> arr = new ArrayList<>();
            arr.add(recognizer.values);
            arr.add(camera.values);
            arr.add(render.values);

            Log.i(TAG, new OutputFormatter(arr).toString());
        }

        void printMinMax() {
            Log.i(TAG, "Minimum");
            printLine(
                values.recognizer.minDuration,
                values.camera.minDuration,
                values.render.minDuration);

            Log.i(TAG, "Maximum");
            printLine(recognizer.maxDuration, camera.maxDuration, render.maxDuration);
        }

        void printAveraged() {
            Log.i(TAG, "Averaged");
            printLine(
                values.recognizer.averaged(), values.camera.averaged(), values.render.averaged());
        }
    }

    private DurationValuesCollection values = new DurationValuesCollection();

    FrameDurationLogger() {
        Log.i(TAG, "Collecting duration values... Press button again to print.");
    }

    void print() {
        if (!values.isValid()) {
            Log.i(TAG, "No data to print");
            return;
        }

        Log.i(TAG, "Frame duration, sec");
        Log.i(TAG, "Recognizer;Camera;Render");
        values.printValues();
        values.printMinMax();
        values.printAveraged();
    }

    @Override
    public void onRecognizerFrameDurationChanged(float instant, float averaged) {
        values.recognizer.newValue(instant);
    }

    @Override
    public void onCameraFrameDurationChanged(float instant, float averaged) {
        values.camera.newValue(instant);
    }

    @Override
    public void onRenderFrameDurationChanged(float instant, float averaged) {
        values.render.newValue(instant);
    }
}
