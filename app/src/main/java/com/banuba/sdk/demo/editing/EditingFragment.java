package com.banuba.sdk.demo.editing;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.SurfaceTexture;
import android.media.ExifInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.banuba.sdk.demo.BanubaClientToken;
import com.banuba.sdk.demo.BottomListAdapter;
import com.banuba.sdk.demo.Effect;
import com.banuba.sdk.demo.MainActivity;
import com.banuba.sdk.demo.PreviewActivity;
import com.banuba.sdk.demo.R;
import com.banuba.sdk.demo.RulerValuesProvider;
import com.banuba.sdk.effect_player.CameraOrientation;
import com.banuba.sdk.effect_player.FrameDataListener;
import com.banuba.sdk.effect_player.ProcessImageParams;
import com.banuba.sdk.entity.RecordedVideoInfo;
import com.banuba.sdk.manager.BanubaSdkManager;
import com.banuba.sdk.manager.BanubaSdkTouchListener;
import com.banuba.sdk.manager.EffectInfo;
import com.banuba.sdk.manager.IEventCallback;
import com.banuba.sdk.types.Data;
import com.banuba.sdk.types.FullImageData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNull;

public class EditingFragment extends Fragment implements IEventCallback {
    private static final String PATH_PARAM = "PATH_PARAM";
    private static final String TAG = "EditingFragment";

    private String mPath;

    private TextureView textureView;
    private View faceNotFoundView;
    private RecyclerView effectsRV;
    private TextView appliedEffectText;

    private List<EffectInfo> effectsInfos;

    private FullImageData imageData;

    private BanubaSdkManager mSdkManager;

    private boolean surfaceCreated = false;

    private EffectInfo selectedEffect;

    private long mBenchmarkTtime;

    private boolean isFirstEffectLoaded = false;
    private FrameDataListener mRulerListener = null;

    public static EditingFragment newInstance(String path) {
        EditingFragment fragment = new EditingFragment();
        Bundle args = new Bundle();
        args.putString(PATH_PARAM, path);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPath = getArguments().getString(PATH_PARAM);
        }
    }

    @Override
    public void onDestroy() {
        recycle();
        super.onDestroy();
    }

    @Override
    public View
    onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_editing, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        BanubaSdkManager.initialize(requireNonNull(getContext()), BanubaClientToken.KEY);
        mSdkManager = new BanubaSdkManager(getContext());
        effectsInfos = mSdkManager.loadEffects();

        try {
            imageData = getImageData();
        } catch (IOException e) {
            e.printStackTrace();
        }

        textureView = view.findViewById(R.id.textureView);
        faceNotFoundView = view.findViewById(R.id.faceFoundView);
        effectsRV = view.findViewById(R.id.effectsList);
        appliedEffectText = view.findViewById(R.id.appliedEffectText);

        view.findViewById(R.id.takePhotoBtn).setOnClickListener((v -> takePhoto()));
        view.findViewById(R.id.backBtn).setOnClickListener((v -> getActivity().onBackPressed()));

        setupTextureView();
        setupEffectsList();
    }

    private void recycle() {
        if (mSdkManager != null) {
            mSdkManager.recycle();
            mSdkManager = null;
        }
    }

    private void setupTextureView() {
        textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                surfaceCreated = true;
                mSdkManager.attachSurface(new Surface(surface));
                mSdkManager.onSurfaceCreated();
                mSdkManager.onSurfaceChanged(-1, width, height);
                mSdkManager.setCallback(EditingFragment.this);

                if (selectedEffect != null) {
                    applyEffect((selectedEffect));
                }
                mSdkManager.getEffectPlayer().playbackPlay();

                startEditingImage();
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                mSdkManager.onSurfaceChanged(-1, width, height);
            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                surfaceCreated = false;
                mSdkManager.getEffectPlayer().playbackPause();
                mSdkManager.onSurfaceDestroyed();
                return true;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {
            }
        });
        textureView.setOnTouchListener(new BanubaSdkTouchListener(getContext(), mSdkManager.getEffectPlayer()));
    }

    private void setupEffectsList() {
        effectsRV.setLayoutManager(
            new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        BottomListAdapter.OnEffectClickListener callback = new BottomListAdapter.OnEffectClickListener() {
            @Override
            public void onAddEffectClick() {
            }

            @Override
            public void onEffectClick(Effect effect) {
                selectedEffect = effect.getEffectInfo();
                applyEffect(effect.getEffectInfo());
            }
        };

        List<Effect> effects = new ArrayList<>();
        for (EffectInfo effectInfo : effectsInfos) {
            effects.add(new Effect(mSdkManager.getResourcesBase() + "/effects/", effectInfo));
        }

        BottomListAdapter adapter = new BottomListAdapter(false, effects, callback);

        effectsRV.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (surfaceCreated) {
            mSdkManager.getEffectPlayer().playbackPlay();
        }
    }

    @Override
    public void onStop() {
        if (mRulerListener != null) {
            mSdkManager.getEffectPlayer().removeFrameDataListener(mRulerListener);
            mRulerListener = null;
        }

        if (surfaceCreated) {
            mSdkManager.getEffectPlayer().playbackPause();
        }
        super.onStop();
    }

    private void startEditingImage() {
        mBenchmarkTtime = System.currentTimeMillis();
        MainActivity.sPhotoRequestTimeMs = System.currentTimeMillis();
        ProcessImageParams params = new ProcessImageParams(false, null, null);
        mSdkManager.startEditingImage(imageData, params);
    }

    private void applyEffect(EffectInfo effect) {
        String fullPath = "effects/" + effect.getPath();
        mSdkManager.loadEffect(fullPath, false);
        String effectName = effect.getName();
        appliedEffectText.setText("Effect: " + effectName);
        mRulerListener = RulerValuesProvider.updateRulerListener(effectName, mSdkManager, mRulerListener);

        startEditingImage(); // todo: create methods to enable recognizer features

        isFirstEffectLoaded = true;
    }

    private FullImageData getImageData() throws IOException {
        final ExifInterface exifInterface = new ExifInterface(mPath);
        final int rotation = exifInterface.getAttributeInt(
            ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        CameraOrientation orientation;
        switch (rotation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                orientation = CameraOrientation.DEG_90;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                orientation = CameraOrientation.DEG_180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                orientation = CameraOrientation.DEG_270;
                break;
            default:
                orientation = CameraOrientation.DEG_0;
                break;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mPath, options);

        final int maxHeight = 3840;
        final int maxWidth = 2400;
        final int srcHeight = options.outHeight;
        final int srcWidth = options.outWidth;
        options = new BitmapFactory.Options();
        if (srcHeight * srcWidth > maxHeight * maxHeight) {
            options.inSampleSize = (int) Math.ceil(Math.max(
                (float) srcHeight / maxHeight,
                (float) srcWidth / maxWidth));
        }
        final Bitmap bitmap = BitmapFactory.decodeFile(mPath, options);
        return new FullImageData(bitmap, new FullImageData.Orientation(orientation));
    }

    private void takePhoto() {
        mBenchmarkTtime = System.currentTimeMillis();
        mSdkManager.takeEditedImage();
    }

    // IEventCallback

    @Override
    public void onEditingModeFaceFound(boolean faceFound) {
        Log.i(TAG, String.format("Start editing time %f s.", (System.currentTimeMillis() - mBenchmarkTtime) / 1000.));
        faceNotFoundView.post(
            () -> faceNotFoundView.setVisibility(faceFound || !isFirstEffectLoaded ? View.GONE : View.VISIBLE));
    }

    @Override
    public void onEditedImageReady(@NonNull Bitmap image) {
        Log.i(TAG, String.format("Editing done %f s.", (System.currentTimeMillis() - mBenchmarkTtime) / 1000.));
        Intent intent = new Intent(getContext(), PreviewActivity.class);
        PreviewActivity.bitmap = image;
        startActivity(intent);
    }

    @Override
    public void onCameraOpenError(@NonNull Throwable error) {
    }

    @Override
    public void onCameraStatus(boolean opened) {
    }

    @Override
    public void onScreenshotReady(@NonNull Bitmap photo) {
    }

    @Override
    public void onHQPhotoReady(@NonNull Bitmap photo) {
    }

    @Override
    public void onVideoRecordingFinished(@NonNull RecordedVideoInfo videoInfo) {
    }

    @Override
    public void onVideoRecordingStatusChange(boolean started) {
    }

    @Override
    public void onImageProcessed(@NonNull Bitmap processedBitmap) {
    }

    @Override
    public void onFrameRendered(@NonNull Data data, int width, int height) {
    }

    @Override
    public void onTextureRendered(int texture, int width, int height, long timestamp, float[] matrix) {
    }
}
