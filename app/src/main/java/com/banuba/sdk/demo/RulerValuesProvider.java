package com.banuba.sdk.demo;

import androidx.annotation.Nullable;

import com.banuba.sdk.effect_player.Effect;
import com.banuba.sdk.effect_player.FrameDataListener;
import com.banuba.sdk.internal.utils.Logger;
import com.banuba.sdk.manager.BanubaSdkManager;
import com.banuba.sdk.types.FrameData;


public class RulerValuesProvider implements FrameDataListener {
    private BanubaSdkManager mSdkManager;

    public RulerValuesProvider(BanubaSdkManager SdkManager) {
        mSdkManager = SdkManager;
    }

    @Override
    public void onFrameDataProcessed(@Nullable FrameData frameData) {
        Effect mCurrentEffect = mSdkManager.getEffectManager().current();

        if (mCurrentEffect != null && frameData != null) {
            String url = mCurrentEffect.url();

            if (!url.isEmpty()) {
                try {
                    float rulerValue = frameData.getRuler();

                    String[] paths = url.split("/");
                    if (paths[paths.length - 1].equals("FaceRuler")) {
                        String params = "{\"text\":\""
                                        + String.valueOf(rulerValue)
                                        + "\",\"R\":\"255\",\"G\":\"0\",\"B\":\"0\",\"A\":\"255\"}";

                        mCurrentEffect.callJsMethod("setFaceDistanceData", params);
                    }

                    if (paths[paths.length - 1].equals("test_Ruler") || paths[paths.length - 1].equals("FaceRuler")) {
                        Logger.i("Ruler value is " + String.valueOf(rulerValue));
                    }
                } catch (Throwable t) {
                    Logger.w("Place your face into frame, please! Error: " + t.toString());
                }
            }
        }
    }

    static public FrameDataListener updateRulerListener(String effectName, BanubaSdkManager SdkManager, FrameDataListener mRulerListener) {
        if (effectName.equals("FaceRuler") || effectName.equals("test_Ruler")) {
            if (mRulerListener == null) {
                mRulerListener = new RulerValuesProvider(SdkManager);
                SdkManager.getEffectPlayer().addFrameDataListener(mRulerListener);
            }
        } else {
            if (mRulerListener != null) {
                SdkManager.getEffectPlayer().removeFrameDataListener(mRulerListener);
                mRulerListener = null;
            }
        }

        return mRulerListener;
    }
}
