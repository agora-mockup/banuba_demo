package com.banuba.sdk.demo;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.widget.ImageView;

import com.banuba.sdk.internal.utils.Logger;

public class PreviewActivity extends Activity {
    /**
     * This bitmap is big enough to pass via Intent extra. Also, we don't want extra Android
     * serialization here.
     */
    public static @Nullable Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        imageView = findViewById(R.id.imageView);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
            Logger.i(
                "PhotoProcessTime %d ms",
                System.currentTimeMillis() - MainActivity.sPhotoRequestTimeMs);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bitmap = null;
    }

    private ImageView imageView;
}
