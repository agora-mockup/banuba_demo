package com.banuba.sdk.demo;

import com.banuba.sdk.manager.EffectInfo;

public class Effect {
    private String additionalPath;
    private EffectInfo effectInfo;

    public Effect(String additionalPath, EffectInfo effectInfo) {
        this.additionalPath = additionalPath;
        this.effectInfo = effectInfo;
    }

    public String getAdditionalPath() {
        return additionalPath;
    }

    public void setAdditionalPath(String additionalPath) {
        this.additionalPath = additionalPath;
    }

    public EffectInfo getEffectInfo() {
        return effectInfo;
    }

    public void setEffectInfo(EffectInfo effectInfo) {
        this.effectInfo = effectInfo;
    }
}
