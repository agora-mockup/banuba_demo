package com.banuba.sdk.demo;

import android.app.Application;

import com.banuba.sdk.manager.BanubaSdkManager;

import static java.util.Objects.requireNonNull;

public class BanubaApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        BanubaSdkManager.initialize(requireNonNull(getApplicationContext()), BanubaClientToken.KEY);
    }

    @Override
    public void onTerminate() {
        BanubaSdkManager.deinitialize();
        super.onTerminate();
    }
}
