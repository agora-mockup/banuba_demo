package com.banuba.sdk.demo.editing;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.banuba.sdk.demo.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder> {
    private List<String> galleryPathList = new ArrayList();
    private OnGalleryItemClickListener onItemClickListener;

    @NonNull
    @Override
    public GalleryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        final View item =
            LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gallery_item, null);
        return new GalleryViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryViewHolder viewHolder, int position) {
        viewHolder.onBind(galleryPathList.get(position));
    }

    @Override
    public int getItemCount() {
        return galleryPathList.size();
    }

    public void updateGalleryItems(List<String> galleryPathList) {
        this.galleryPathList.clear();
        this.galleryPathList.addAll(galleryPathList);
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnGalleryItemClickListener listener) {
        onItemClickListener = listener;
    }

    protected class GalleryViewHolder extends RecyclerView.ViewHolder {
        private final ImageView galleryImage;

        public GalleryViewHolder(@NonNull View itemView) {
            super(itemView);
            galleryImage = itemView.findViewById(R.id.galleryImage);
        }

        void onBind(String path) {
            Glide.with(galleryImage.getContext())
                .load(path)
                .placeholder(R.drawable.ic_loading)
                .into(galleryImage);
            galleryImage.setOnClickListener((v -> onItemClickListener.onGalleryItemClick(path)));
        }
    }

    protected interface OnGalleryItemClickListener {
        void onGalleryItemClick(String path);
    }
}
